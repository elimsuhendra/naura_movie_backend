<?php
//Main route
Route::get('/', 'IndexController@index');
Route::post('reset', 'ForgotController@reset');
Route::get('forgot/{token}', 'ForgotController@index');
Route::get('cart', 'CheckoutController@cart');
Route::get('contact', 'PagesController@contact');
Route::get('login', 'AccountController@login');
Route::get('product/{id}', 'ProductController@index');
Route::get('thanks_page', 'PagesController@thanks_page');


Route::get('merchants', 'PagesController@merchant');
Route::post('filter', 'PagesController@merchant');
Route::get('merchants/{id}', 'PagesController@merchant_detail');
Route::get('our-company', 'PagesController@company');
Route::get('our-services', 'PagesController@services');
Route::get('news', 'PagesController@news');
Route::get('news/{slug}', 'PagesController@detail_news');
Route::get('contact', 'PagesController@contact');

Route::get('promo/{slug}', 'PagesController@promo');
Route::get('our-clients', 'PagesController@our_clients');

Route::get('apply', 'PagesController@apply');
Route::post('apply', 'PagesController@apply_sub');

Route::group(['prefix' => 'parts'], function(){
	Route::get('mini-cart', 'CheckoutController@miniCart');
	Route::get('cart', 'CheckoutController@cartParts');
});

Route::group(['prefix' => 'v1'], function(){
	Route::post('add-cart', 'ProductController@addCart');
	Route::post('remove-cart', 'ProductController@removeCart');
	Route::get('total-cart', 'ProductController@totalCart');
});