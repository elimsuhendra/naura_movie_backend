<?php
	Route::get('login','LoginController@index');
	Route::post('cek_login','LoginController@cek_login');
	Route::post('forgot_password','LoginController@forgot_password');
	Route::get('logout','LoginController@logout');
	
	Route::get('profile', 'LoginController@profile');
	Route::post('update_profile', 'LoginController@update_profile');
	Route::get('change-language/{id}','LoginController@change_language');
	Route::get('thanks_page','LoginController@thanks_page');

	//Main route
	Route::get('/', 'IndexController@index');

	Route::group(['prefix' => 'users'], function(){
		Route::resource('user', 'UserController');
		Route::any('user/ext/{action}','UserController@ext');
		Route::get('user/get_store/{id}','UserController@get_store');

		Route::resource('access-right', 'UseraccessController');
		Route::any('access-right/ext/{action}','UseraccessController@ext');

		Route::resource('members', 'MembersController');
		Route::any('members/ext/{action}','MembersController@ext');
	});

	Route::group(['prefix' => 'cinemas'], function(){
		Route::resource('service-cinema', 'CinemaServiceController');
		Route::any('service-cinema/ext/{action}','CinemaServiceController@ext');

		Route::resource('cinema', 'CinemaController');
		Route::any('cinema/ext/{action}','CinemaController@ext');

		Route::resource('ticket', 'TicketController');
		Route::any('ticket/ext/{action}','TicketController@ext');

		Route::resource('preorder', 'PreorderController');
		Route::any('preorder/ext/{action}','PreorderController@ext');

	});


	Route::group(['prefix' => 'account-&-payment'], function(){
		Route::resource('bank-account', 'BankAccountController');
		Route::any('bank-account/ext/{action}','BankAccountController@ext');
	});

	Route::group(['prefix' => 'mobile'], function(){
		Route::resource('banner', 'BannerController');
		Route::any('banner/ext/{action}','BannerController@ext');

		// Route::resource('merchant-category', 'MerchantCategoryController');
		// Route::any('merchant-category/ext/{action}','MerchantCategoryController@ext');

		// Route::get('manage-merchant/get_merchant_category/{par}', 'MerchantController@get_merchant_category');
		// Route::get('manage-merchant/get_merchant_location/{par}', 'MerchantController@get_merchant_location');
	});

	Route::group(['prefix' => 'administration'], function(){
		Route::resource('user-access', 'UseraccessController');
		Route::any('user-access/ext/{action}','UseraccessController@ext');

		Route::resource('user', 'UserController');
		Route::any('user/ext/{action}','UserController@ext');

		Route::resource('customer', 'CustomerController');
		Route::any('customer/ext/{action}','CustomerController@ext');
	});

	Route::group(['prefix' => 'preferences'], function(){
		Route::get('general-settings', 'ConfigController@index');
		Route::post('general-settings/update', 'ConfigController@update');
	});

	//Main services for ajax outside builder
	Route::any('services','ServicesController@index');