@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
	<div class="portlet light bordered">
    	<div class="portlet-title">
			<div class="caption font-green">
				<i class="icon-layers font-green title-icon"></i>
				<span class="caption-subject bold uppercase"> {{$title}}</span>
			</div>
			<div class="actions">
				<a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
			</div>
    	</div>
    	<div class="portlet-body form">
      		@include('admin.includes.errors')
  			<div class="row">
  				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'promo_name','label' => 'Promo Name','value' => (old('name') ? old('name') : $promo->promo_name),'attribute' => 'required autofocus','form_class' => 'col-md-12', 'class' => 'name'])!!}

				{!!view($view_path.'.builder.textarea',['type' => 'textarea','name' => 'promo_detail','label' => 'Promo Detail','value' => (old('username') ? old('username') : $promo->promo_detail),'attribute' => 'required autofocus','form_class' => 'col-md-12', 'class' => 'username'])!!}

				{!!view($view_path.'.builder.file',['name' => 'image','label' => 'Image','value' => $promo->image,'type' => 'file','file_opt' => ['path' => $image_path],'upload_type' => 'single-image','class' => 'col-md-12','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 100 x 500 px','form_class' => 'col-md-12', 'required' => 'n'])!!}
				
				{!!view($view_path.'.builder.text',['name' => 'start_date_promo','label' => 'Start Date Promo','value' => (old('start_date_promo') ? old('start_date_promo') : $promo->start_date),'class' => 'datepicker','form_class' => 'col-md-6','attribute' => 'required readonly'])!!}

				{!!view($view_path.'.builder.text',['name' => 'end_date_promo','label' => 'End Date Promo','value' => (old('end_date_promo') ? old('end_date_promo') : $promo->end_date),'class' => 'datepicker','form_class' => 'col-md-6','attribute' => 'required readonly'])!!}

				{!!view($view_path.'.builder.text',['name' => 'publish_time','label' => 'Publish Time','value' => date('H:i'),'attribute' => 'required readonly','form_class' => 'col-md-12', 'validation' => 'required', 'class' => 'timepicker', 'onchange' => ''])!!}

			 	<div class="col-md-12">
				  	<div class="form-group" style={{in_array(auth()->guard($guard)->user()->user_access_id,[1,12]) ? '' : "display:none;"}}>
		              <label for="tag">Store <span class="required" aria-required="true">*</span></label>
		              <select class="select2" name="store_allowed[]" multiple="multiple">
		                @if(!is_array(json_decode(auth()->guard($guard)->user()->store_id)) && in_array(auth()->guard($guard)->user()->store_id,[0,1]))
		                  <option value="0" {{old('store_allowed') ? (in_array(0,old('store_allowed')) ? 'selected' : '') : ''}}>-- All Store --</option>
		                @endif

		                @foreach($store as $s)
		                  <option value="{{$s->id}}" {{old('store_allowed') ? (in_array($s->id,old('store_allowed')) ? 'selected' : '') : ($selected_store != null ? (in_array($s->id,$selected_store) ? 'selected' : ''): '')}}>{{$s->store_name}}</option>
		                @endforeach
		              </select>
		            </div>
		        </div>

	             {!!view($view_path.'.builder.radio',['type' => 'radio','data' => ['y' => 'Active','n' => 'Not Active'],'name' => 'status','label' => 'Status','form_class' => 'col-md-6','value' => $promo->status])!!}
			</div>	
			{!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
		</div>
	</div>
</form>
@push('custom_scripts')
	<script>
		$(document).ready(function(){
			$('input,select,textarea,button.remove-single-image').prop('disabled',true);
		});
	</script>
@endpush
@endsection