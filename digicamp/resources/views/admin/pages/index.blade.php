@extends($view_path.'.layouts.master')
@section('content')
@stack('scripts')
<script src="{{asset('components/plugins/amcharts/amcharts/amcharts.js')}}"></script>
<script src="{{asset('components/plugins/amcharts/amcharts/serial.js')}}"></script>
<!-- Load the JavaScript API client and Sign-in library. -->
<script src="https://apis.google.com/js/client:platform.js"></script>
@push('styles')

@endpush
<!-- <div class="row">
	<div class="col-md-12 dashboard coming-soon">
		<img class="img-responsive" src="{{asset('components/back/images/admin/dashboard.jpg')}}">
	</div>
</div> -->

	<div class="row">
      	<div class="col-md-6 col-sm-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-blue"></i>
                        <span class="caption-subject font-blue bold uppercase">Top Ticket Sales By City</span>
                    </div>
                    <div class="actions">
                        <!-- <div class="btn-group">
                            <a class="btn btn-sm blue btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Filter By
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                                <label>
                                    <input type="checkbox" /> Finance</label>
                                <label>
                                    <input type="checkbox" checked="" /> Membership</label>
                                <label>
                                    <input type="checkbox" /> Customer Support</label>
                                <label>
                                    <input type="checkbox" checked="" /> HR</label>
                                <label>
                                    <input type="checkbox" /> System</label>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th> # </th>
                                        <th> City </th>
                                        <th> Ready for Redemeed </th>
                                        <th> Redemeed </th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@if(count($preorder_1) > 0)
	                                	@foreach($preorder_1 as $key => $pr1)
	                                        <tr>
	                                            <td> {{$key + 1}} </td>
	                                            <td> {{$pr1->city_name}} </td>
	                                            @if($preorder_2)
	                                                @php
	                                                	$total = 0;
	                                                    foreach($preorder_2 as $key => $pr2){
	                                                    	if($pr2->bioskop_name == $pr1->bioskop_name){
	                                                    		$total = $pr2->total;
	                                                    	}
	                                                    }
	                                                	
	                                                @endphp
	                                            @else
	                                             	@php
	                                             		$total = 0;
	                                             	@endphp	
	                                            @endif
	                                            <td> {{$pr1->total}} </td>
	                                            <td>{{$total}}</td>
	                                        </tr>
	                                    @endforeach
	                                @elseif(count($preorder_2) > 0)
	                                	@foreach($preorder_2 as $key => $pr2)
	                                        <tr>
	                                            <td> {{$key + 1}} </td>
	                                            <td> {{$pr2->city_name}} </td>
	                                            <td> 0 </td>
	                                            <td> {{$pr2->total}} </td>
	                                        </tr>
	                                    @endforeach
	                                @endif
                                    <!-- <tr>
                                        <td> 2 </td>
                                        <td> Table cell </td>
                                        <td> Table cell </td>
                                        <td> Table cell </td>
                                    </tr>
                                    <tr>
                                        <td> 3 </td>
                                        <td> Table cell </td>
                                        <td> Table cell </td>
                                        <td> Table cell </td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <div class="col-md-6 col-sm-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-blue"></i>
                        <span class="caption-subject font-blue bold uppercase">Top Ticket Sales By Bioskop</span>
                    </div>
                    <div class="actions">
                        <!-- <div class="btn-group">
                            <a class="btn btn-sm blue btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Filter By
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                                <label>
                                    <input type="checkbox" /> Finance</label>
                                <label>
                                    <input type="checkbox" checked="" /> Membership</label>
                                <label>
                                    <input type="checkbox" /> Customer Support</label>
                                <label>
                                    <input type="checkbox" checked="" /> HR</label>
                                <label>
                                    <input type="checkbox" /> System</label>
                            </div>
                        </div> -->
                    </div>
                </div>
             
                <div class="portlet-body">
                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th> # </th>
                                        <th> Bioskop </th>
                                        <th> Ready for Redemeed </th>
                                        <th> Redemeed </th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@if(count($preorder_1_1) > 0)
	                                	@foreach($preorder_1_1 as $key => $pr1_1)
	                                        <tr>
	                                            <td> {{$key + 1}} </td>
	                                            <td> {{$pr1_1->bioskop_name}} </td>
	                                            @if($preorder_2_1)
	                                                @php
	                                                	$total = 0;
	                                                    foreach($preorder_2_1 as $key => $pr2_1){
	                                                    	if($pr2_1->bioskop_name == $pr1_1->bioskop_name){
	                                                    		$total = $pr2_1->total;
	                                                    	}
	                                                    }
	                                                	
	                                                @endphp
	                                            @else
	                                             	@php
	                                             		$total = 0;
	                                             	@endphp	
	                                            @endif
	                                            <td> {{$pr1_1->total}} </td>
	                                            <td>{{$total}}</td>
	                                        </tr>
	                                    @endforeach
	                                @elseif(count($preorder_2_1) > 0)
	                                	@foreach($preorder_2_1 as $key => $pr2_1)
	                                        <tr>
	                                            <td> {{$key + 1}} </td>
	                                            <td> {{$pr2_1->city_name}} </td>
	                                            <td> 0 </td>
	                                            <td> {{$pr2_1->total}} </td>
	                                        </tr>
	                                    @endforeach
	                                @endif
                                    <!-- <tr>
                                        <td> 2 </td>
                                        <td> Table cell </td>
                                        <td> Table cell </td>
                                        <td> Table cell </td>
                                    </tr>
                                    <tr>
                                        <td> 3 </td>
                                        <td> Table cell </td>
                                        <td> Table cell </td>
                                        <td> Table cell </td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-6 col-sm-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-blue"></i>
                        <span class="caption-subject font-blue bold uppercase">Top Ticket Sales In {{$cur_month}}</span>
                    </div>
                    <div class="actions">
                        <!-- <div class="btn-group">
                            <a class="btn btn-sm blue btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Filter By
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                                <label>
                                    <input type="checkbox" /> Finance</label>
                                <label>
                                    <input type="checkbox" checked="" /> Membership</label>
                                <label>
                                    <input type="checkbox" /> Customer Support</label>
                                <label>
                                    <input type="checkbox" checked="" /> HR</label>
                                <label>
                                    <input type="checkbox" /> System</label>
                            </div>
                        </div> -->
                    </div>
                </div>
             
                <div class="portlet-body">
                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th> # </th>
                                        <th> Bioskop </th>
                                        <th> Ready for Redemeed </th>
                                        <th> Redemeed </th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@if(count($preorder_1_2) > 0)
	                                	@foreach($preorder_1_2 as $key => $pr1_2)
	                                        <tr>
	                                            <td> {{$key + 1}} </td>
	                                            <td> {{$pr1_2->bioskop_name}} </td>
	                                            @if($preorder_2_2)
	                                                @php
	                                                	$total = 0;
	                                                    foreach($preorder_2_2 as $key => $pr2_2){
	                                                    	if($pr2_2->bioskop_name == $pr1_2->bioskop_name){
	                                                    		$total = $pr2_2->total;
	                                                    	}
	                                                    }
	                                                	
	                                                @endphp
	                                            @else
	                                             	@php
	                                             		$total = 0;
	                                             	@endphp	
	                                            @endif
	                                            <td> {{$pr1_2->total}} </td>
	                                            <td>{{$total}}</td>
	                                        </tr>
	                                    @endforeach
	                                @elseif(count($preorder_2_2) > 0)
	                                	@foreach($preorder_2_2 as $key => $pr2_2)
		                                	<tr>
	                                            <td> {{$key + 1}} </td>
	                                            <td> {{$pr2_2->city_name}} </td>
	                                            <td> 0 </td>
	                                            <td> {{$pr2_2->total}} </td>
	                                        </tr>
	                                    @endforeach
	                                @endif
                                    <!-- <tr>
                                        <td> 2 </td>
                                        <td> Table cell </td>
                                        <td> Table cell </td>
                                        <td> Table cell </td>
                                    </tr>
                                    <tr>
                                        <td> 3 </td>
                                        <td> Table cell </td>
                                        <td> Table cell </td>
                                        <td> Table cell </td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END ROW -->

@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){
        
    });
</script>
@endpush
@endsection
