@extends($view_path.'.layouts.master')
@section('content')
@section('content')
<style>

</style>

@push('styles')
<style>

</style>

<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">        
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'title','label' => 'Title','value' => (old('title') ? old('title') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'title'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'slug','label' => 'Slug','value' => (old('slug') ? old('slug') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'slug'])!!}

        {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : ''),'attribute' => ' autofocus','form_class' => 'col-md-12','class' => 'editor'])!!}

        <div class="form-group col-md-2" style="">
              <div class="md-checkbox">
                <label>Headline News</label>
              <input type="checkbox" id="checkbox_form_1" class="md-check headline-news" name="headline_news" value="y">
              <label for="checkbox_form_1">
                <span></span>
                    <span class="check"></span>
                    <span class="box"></span>
              </label>
            </div>
        </div>

        <div class="form-group col-md-2" style="">
              <div class="md-checkbox">
                <label>Sticky News</label>
              <input type="checkbox" id="checkbox_form_2" class="md-check sticky-news" name="sticky_news" value="y">
              <label for="checkbox_form_2">
                <span></span>
                    <span class="check"></span>
                    <span class="box"></span>
              </label>
            </div>
        </div>

        <div class="form-group form-md-line-input col-md-12">
            <label>Image</label><br>
            <label class="btn green input-file-label-image_card">
              <input type="file" class="form-control col-md-12 single-image" name="image"> Pilih File
            </label>
                <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="image">Hapus</button>
              <input type="hidden" name="remove-single-image-image" value="n">
              <br>
            <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

            <div class="form-group single-image-image col-md-12">
              <img src="{{asset($image_path2)}}/none.png" class="img-responsive thumbnail single-image-thumbnail">
            </div>
        </div>

       
    </div>

    <div class="row">
        <div class="col-md-12">
          <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
          <h4 >Meta Data</h4>
          </div>
            <small>Note: Leave blank to use default value</small>
        </div>
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_title','label' => 'Meta Title','value' => (old('meta_title') ? old('meta_title') : ''),'attribute' => 'autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_keyword','label' => 'Meta Keyword','value' => (old('meta_keyword') ? old('meta_keyword') : ''),'attribute' => 'autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_description','label' => 'Meta Description','value' => (old('meta_description') ? old('meta_description') : ''),'attribute' => 'autofocus','form_class' => 'col-md-12'])!!}

         <div class="col-md-12 actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
        </div>
    </div>
  </div>
</form>

@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $('.title').keyup(function(){ 
      var slug = convertToSlug($(this).val());
      console.log(slug);
      $(".slug").val(slug);    
    });

    function convertToSlug(Text)
    {
        return Text
            .toLowerCase()
            .replace(/ /g,'-')
            .replace(/[^\w-]+/g,'')
            ;
    }
  </script>
@endpush
@endsection
