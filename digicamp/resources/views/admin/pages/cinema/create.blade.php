@extends($view_path.'.layouts.master')
@section('content')
@section('content')
<style>

</style>

@push('styles')
<style>

</style>

<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">        
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'bioskop_name','label' => 'Bioskop Name','value' => (old('bioskop_name') ? old('bioskop_name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'bioskop_name'])!!}

          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'cinema_code','label' => 'Cinema Code','value' => (old('cinema_code') ? old('cinema_code') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'cinema_code'])!!}

          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'phone'])!!}

          {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : ''),'attribute' => ' autofocus','form_class' => 'col-md-12','class' => ''])!!}

           {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'address','label' => 'Address','value' => (old('address') ? old('address') : ''),'attribute' => ' autofocus','form_class' => 'col-md-12','class' => ''])!!}

          <div class="form-group col-md-6">
                <label for="tag">Province</label>
                <select class="select2 province" name="province">
                  @foreach($data1 as $key => $m)
                    <option value="{{$key}}">{{$m}}</option>
                  @endforeach
                </select>
          </div>

          <input type="hidden" id="arr_city" value="{{$data2}}">
           <div class="form-group col-md-6 city2">
                <label for="tag">City</label>
                <select class="select2 city" name="city">
                    <!-- <option value=""></option> -->
                </select>
          </div>        

    <div class="row">
        <!-- <div class="col-md-12">
          <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
          <h4 >Meta Data</h4>
          </div>
            <small>Note: Leave blank to use default value</small>
        </div>
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_title','label' => 'Meta Title','value' => (old('meta_title') ? old('meta_title') : ''),'attribute' => 'autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_keyword','label' => 'Meta Keyword','value' => (old('meta_keyword') ? old('meta_keyword') : ''),'attribute' => 'autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_description','label' => 'Meta Description','value' => (old('meta_description') ? old('meta_description') : ''),'attribute' => 'autofocus','form_class' => 'col-md-12'])!!} -->

         <div class="col-md-12 actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
        </div>
    </div>
  </div>
</form>

@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    // $(document).ready(function(){
        $('.title').keyup(function(){ 
          var slug = convertToSlug($(this).val());
          console.log(slug);
          $(".slug").val(slug);    
        });

        function convertToSlug(Text)
        {
            return Text
                .toLowerCase()
                .replace(/ /g,'-')
                .replace(/[^\w-]+/g,'')
                ;
        }

        getCity();
         $('.province').change(function(){
            getCity();
         });

        function getCity(){
            var id = $('.province').val();
            var arr_city = JSON.parse($('#arr_city').val());
            var text = '';
            for(var i=0; i<arr_city.length; i++){
                if(arr_city[i]['province_id'] == id){
                    text += '<option value="'+arr_city[i]['id']+'">'+arr_city[i]['name']+'</option>';
                }
            }
            // text += '';
            console.log(text);
            $('.city2 .select2-container--bootstrap .select2-selection--single .select2-selection__rendered').text('');
            $('.city').text('');
            $('.city').val('');
            // $('.city').prop('disabled', false);
            $('.city').append(text);
        }
    // });
  </script>
@endpush
@endsection
