@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$merchant->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">

       
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'merchant_name','label' => 'Name','value' => (old('name') ? old('name') : $merchant->merchant_name),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

         {!!view($view_path.'.builder.text',['type' => 'email','name' => 'email','label' => 'Email','value' => (old('email') ? old('email') : $merchant->email),'attribute' => 'required','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : $merchant->phone),'attribute' => 'required','form_class' => 'col-md-6'])!!}


        {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'address','label' => 'Headquarter Address','value' => (old('address') ? old('address') : $merchant->address),'attribute' => 'required','form_class' => 'col-md-6'])!!}

         {!!view($view_path.'.builder.select',['name' => 'city','label' => 'City','value' => (old('city') ? old('city') : $merchant->id_city),'attribute' => 'required','form_class' => 'col-md-6', 'data' => $data2, 'class' => 'select2'])!!}

         {!!view($view_path.'.builder.select',['name' => 'merchant_category','label' => 'Category Merchant','value' => (old('merchant_category') ? old('merchant_category') : $merchant->id_merchant_category),'attribute' => 'required','form_class' => 'col-md-6', 'data' => $data1, 'class' => 'select2'])!!}

          <div class="form-group form-md-line-input col-md-12">
              <label>Logo</label><br>
              <label class="btn green input-file-label-logo">
                <input type="file" class="form-control col-md-12 single-image" name="logo"> Pilih File
              </label>
                  <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="logo">Hapus</button>
                <input type="hidden" name="remove-single-image-logo" value="n">
                <br>
              <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

              <div class="form-group single-image-logo col-md-12">
                <img src="{{asset($image_path.'/'.$merchant->logo)}}" class="img-responsive thumbnail single-image-thumbnail">
              </div>
          </div>
      </div>

      <div class="row">
            <div class="form-group col-md-12">
              <label for="tag" class="sub-title">Outlet</label>
            </div>
              <input type="hidden" id="data_outlet" value="">

              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'outlet_name','label' => 'Outlet Name','value' => (old('outlet_name') ? old('outlet_name') : ''),'attribute' => '','form_class' => 'col-md-6', 'class'=>'outlet_name'])!!}

              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'outlet_email','label' => 'Outlet Email','value' => (old('outlet_email') ? old('outlet_email') : ''),'attribute' => '','form_class' => 'col-md-6', 'class'=>'outlet_email'])!!}

              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'outlet_phone','label' => 'Outlet Phone','value' => (old('outlet_phone') ? old('outlet_phone') : ''),'attribute' => '','form_class' => 'col-md-6', 'class'=>'outlet_phone'])!!}

              {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'outlet_address','label' => 'Outlet Address','value' => (old('outlet_address') ? old('outlet_address') : ''),'attribute' => '','form_class' => 'col-md-6','class'=>'outlet_address'])!!}

            <div class="col-md-12">
              <!-- {!!view($view_path.'.builder.button',['type' => 'button','label' => 'Add','class' => 'add-outlet'])!!} -->
            </div>
          <hr/>
          <div class="table-responsive redeem-auto col-md-12">
            <table class="table table-bordered">
              <thead>
                <th>Outlet Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Address</th>
                <th>Action</th>
              </thead>
              <tbody class="outlet-data">
                @foreach($data3 as $d3)
                  <tr>                                  
                    <td>{{$d3->outlet_name}}</td>
                    <td>{{$d3->phone}}</td>
                    <td>{{$d3->email}}</td>
                    <td>{{$d3->address}}  
                    <input type="hidden" name="outlet_name2[]" value="{{$d3->outlet_name}}">
                    <input type="hidden" name="outlet_address2[]" value="{{$d3->phone}}"><input type="hidden" name="outlet_phone2[]" value="{{$d3->email}}"><input type="hidden" name="outlet_email2[]" value="{{$d3->address}}"></td>
                    <td><button type="button" class="btn btn-danger delete-outlet"><i class="fa fa-trash"></i></button></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
      </div>

     <!-- <div class="row">
            <div class="form-group col-md-12">
              <label for="tag">Promo</label>
            </div>
            
              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'promo_name','label' => 'Promo Name','value' => (old('promo_name') ? old('promo_name') : ''),'attribute' => 'required','form_class' => 'col-md-6', 'class'=>'promo_name'])!!}

              {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'promo_description','label' => 'Promo Description','value' => (old('description') ? old('description') : ''),'attribute' => 'required','form_class' => 'col-md-12','class'=>'promo_description'])!!}

              {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'term_condition','label' => 'Term & Condition','value' => (old('description') ? old('description') : ''),'attribute' => 'required','form_class' => 'col-md-12','class'=>'promo_description'])!!}

            <div class="form-group form-md-line-input col-md-12">
                <label>Image</label><br>
                <label class="btn green input-file-label-image_card">
                  <input type="file" class="form-control col-md-12 single-image promo_image" name="promo_image"> Pilih File
                </label>
                    <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="proo_image">Hapus</button>
                  <input type="hidden" name="remove-single-image-promo_image" value="n">
                  <br>
                <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

                <div class="form-group single-image_promo_image col-md-12">
                  <img src="{{asset($image_path2)}}/none.png" class="img-responsive thumbnail single-image-thumbnail img-promo_image">
                </div>
            </div>

            <div class="col-md-12">
              {!!view($view_path.'.builder.button',['type' => 'button','label' => 'Add','class' => 'add-promo'])!!}
            </div>
          <hr/>
          <div class="table-responsive redeem-auto col-md-12">
            <table class="table table-bordered">
              <thead>
                <th>Promo Name</th>
                <th>Promo Description</th>
                <th>Term & Condition</th>
                <th>Promo Image</th>
                <th>Action</th>
              </thead>
              <tbody class="outlet-data">

              </tbody>
            </table>
          </div>
      </div> -->
      <!-- {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!} -->
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      $('input,select,checkbox,button.remove-single-image,delete-outlet').attr('disabled',true);
      $(document).on('click','.add-outlet',function(){
        var outlet_name2       = $('.outlet_name').val();
        var outlet_address2    = $('.outlet_address').val();
        var outlet_phone2      = $('.outlet_phone').val();
        var outlet_email2      = $('.outlet_email').val();

        var error       = '';
        var returnfalse =   0;
       if(outlet_name2 == ""){
            returnfalse = 1;
            error = 'Outlet Name required !';
        }else if(outlet_address2 == ""){
            returnfalse = 1;
            error = 'Outlet Address required !';
        }

        if(returnfalse == 1){
          alert(error);
        }else{
            var temp        = '<tr><td>'+outlet_name2+'</td><td>'+outlet_phone2+'</td><td>'+outlet_email2+'</td><td>'+outlet_address2+'<input type="hidden" name="outlet_name2[]" value="'+outlet_name2+'"><input type="hidden" name="outlet_address2[]" value="'+outlet_address2+'"><input type="hidden" name="outlet_phone2[]" value="'+outlet_phone2+'"><input type="hidden" name="outlet_email2[]" value="'+outlet_email2+'"></td><td><button type="button" class="btn btn-danger delete-outlet"><i class="fa fa-trash"></i></button></td></tr>';
            $('.outlet-data').append(temp);
        }
        
      });

      // $(document).on('click','.add-promo',function(){
      //   var data1      = $('.promo_name').val();
      //   var data2      = $('.promo_descripton').val();
      //   var data3      = $('.term_condition').val();
      //   var src        = $('.img-promo_image').attr("src");
      //   var img        = "<img src='"+src+"' class='img-responsive thumbnail single-image-thumbnail'>";
      //   var file       = $(".promo_image")[0].files;


      //   var temp        = '<tr><td>'+data1+'</td><td>'+data2+'</td><td>'+data3+'</td><td>'+file+'<input type="hidden" name="promo_name2[]" value="'+data1+'"><input type="hidden" name="promo_descripton2[]" value="'+promo_descripton2+'"><input type="hidden" name="term_condition2[]" value="'+term_condition2+'"><input type="hidden" name="term_condition2[]" value="'+term_condition2+'"></td><td><button type="button" class="btn btn-danger delete-outlet"><i class="fa fa-trash"></i></button></td></tr>';
      //   $('.outlet-data').append(temp);
      // });

      $(document).on('click','.delete-outlet',function(){
        $(this).closest('tr').remove();
      })
    });
  </script>
@endpush
@endsection
