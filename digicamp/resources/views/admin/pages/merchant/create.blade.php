@extends($view_path.'.layouts.master')
@section('content')
@section('content')
<style>
 /* .single-image-card{
    max-width: 1024px !important;
    max-height: 714px !important;
  }*/
</style>
<!-- croppie -->
<link rel = "stylesheet" href="{{asset('components/back/css/croppie.css')}}" type="text/css">
<!-- <link rel = "stylesheet" href="{{asset('components/back/css/demo.css')}}" type="text/css"> -->
<!-- croppie -->
@push('styles')
<style>
  .modal-dialog {
    width: 1300px;
    height: 100%;
    margin: 0;
    padding: 0;
  }

  .modal-content {
    height: auto;
    min-height: 100%;
    border-radius: 0;
  }
  
  .canvas-image_card{
    width: 1100px;
    height: 800px;
  }
</style>

<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">        
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'merchant_name','label' => 'Name','value' => (old('name') ? old('name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'email','name' => 'email','label' => 'Email','value' => (old('email') ? old('email') : ''),'attribute' => 'required','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : ''),'attribute' => 'required','form_class' => 'col-md-6'])!!}


        {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'address','label' => 'Headquarter Address','value' => (old('address') ? old('address') : ''),'attribute' => 'required','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.select',['name' => 'city','label' => 'City','value' => (old('city') ? old('city') : '1'),'attribute' => 'required','form_class' => 'col-md-6', 'data' => $data2, 'class' => 'select2'])!!}

         {!!view($view_path.'.builder.select',['name' => 'merchant_category','label' => 'Category Merchant','value' => (old('merchant_category') ? old('merchant_category') : '1'),'attribute' => 'required','form_class' => 'col-md-6', 'data' => $data1, 'class' => 'select2'])!!}

          <div class="form-group col-md-12" style="">
            <div class="form-group col-md-2" style="">
                  <div class="md-checkbox">
                    <label>Sticky</label>
                  <input type="checkbox" id="checkbox_form_2" class="md-check sticky-news" name="sticky" value="y" {{isset($merchant->sticky) ? ($merchant->sticky == 'y' ? 'checked' : '') : ''}}>
                  <label for="checkbox_form_2">
                    <span></span>
                        <span class="check"></span>
                        <span class="box"></span>
                  </label>
                </div>
            </div>
          </div>

        <div class="form-group form-md-line-input col-md-12">
            <label>Logo</label><br>
            <label class="btn green input-file-label-logo">
              <input type="file" class="form-control col-md-12 single-image" name="logo"> Pilih File
            </label>
                <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="logo">Hapus</button>
              <input type="hidden" name="remove-single-image-logo" value="n">
              <br>
            <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

            <div class="form-group single-image-logo col-md-12">
              <img src="{{asset($image_path2)}}/none.png" class="img-responsive thumbnail single-image-thumbnail">
            </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="form-group form-md-line-input" style="border-bottom: 1px solid #eef1f5;">
          <h4 >Meta Data</h4>
          </div>
            <small>Note: Leave blank to use default value</small>
        </div>
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_title','label' => 'Meta Title','value' => (old('meta_title') ? old('meta_title') : ''),'attribute' => 'autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_keyword','label' => 'Meta Keyword','value' => (old('meta_keyword') ? old('meta_keyword') : ''),'attribute' => 'autofocus','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'meta_description','label' => 'Meta Description','value' => (old('meta_description') ? old('meta_description') : ''),'attribute' => 'autofocus','form_class' => 'col-md-12'])!!}
      </div>
      
      <div class="row">
          <div class="col-md-12 actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
        </div>
      </div>
  </div>
</form>

@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    var map;
    $(document).ready(function(){
      // $("#image-card").hide();
      // $("#image-stamp").hide();

      // $(document).on('change','.select_membership',function(res){
      //     var res2 = $(".select_membership").val();
      //     console.log(res2);
      //     $("#image-card").hide();
      //     $("#image-stamp").hide();
      //     showHideImage(res2)
      // });

      // showHideImage($(".select_membership").val())

      // $(document).on('keyup','.region_kecamatan_id',function(res){
      //   console.log(res);
      //   var text = $(".region_kecamatan_id").val();
      //   if(text.length != undefined && text.length >= 5){
      //     getKecamatan(text);
      //   } 
      // });
      //   // var data = [];
      //  // console.log(data);
      
      // function getKecamatan(text){
      //   // var merchant_id = $('.select-merchant option:selected').val();
      //   // $(".select-store").children('option:not(:first)').remove(); 
      //   // $("#select2-store_id-eb-container").text("");   
      //   var url    = $.root() + 'merchants/store/get_kecamatan/' + text;
      //   console.log(url);
      //     $.ajax({
      //     url: url,
      //     type: "GET",
      //     dataType: 'json',
      //     // data: par
      //     }).done(function(msg) {
      //         var data = [];
      //         $.each(msg, function( index, value ) {
      //           var arr={
      //             'value': index,
      //             'label': value
      //           }

      //           data.push(arr);
      //         });
      //         console.log(data);

      //         $( ".region_kecamatan_id" ).autocomplete({
      //             minLength: 0,
      //             source: data,
      //             focus: function( event, ui ) {
      //               console.log(ui);
      //               $( ".region_kecamatan_id" ).val( ui.item.label );
      //               return false;
      //             },
      //             select: function( event, ui ) {
      //               console.log(ui.item);
      //               $( ".region_kecamatan_id" ).val( ui.item.label );
      //               $( "#kecamatan_id" ).val( ui.item.value );

      //               return false;
      //             }
      //         })
      //     });
      // }

      // function showHideImage(res2){
      //   if(res2 == '3'){
      //       $("#image-card").show();
      //       $("#image-stamp").show();
      //       $("#point_label").text('Stamp');
      //   }else if(res2 == '1' || res2 == '2'){
      //       $("#image-card").show();
      //        $("#point_label").text('Point');
      //   }
      // }

      // setTimeout(function(){ 
      //     var lat = $('#latbox').val();
      //     var lng = $('#lngbox').val();

      //     google.maps.event.trigger(map, 'resize'); 
        
      //     Markerlatlng = new google.maps.LatLng(lat, lng);

      //     map.setCenter(Markerlatlng); // setCenter takes a LatLng object
      //   }, 2500);

      // document.getElementById("price").onblur =function (){    

      //   //number-format the user input
      //   this.value = parseFloat(this.value.replace(/,/g, ""))
      //               .toFixed(2)
      //               .toString()
      //               .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      // }
     
      // function popupResult(result) {
      //   var html;
      //   if (result.html) {
      //     html = result.html;
      //   }
      //   if (result.src) {
      //     html = '<img src="' + result.src + '" />';
      //   }
      //   swal({
      //     title: '',
      //     html: true,
      //     text: html,
      //     allowOutsideClick: true
      //   });
      //   setTimeout(function(){
      //     $('.sweet-alert').css('margin', function() {
      //       var top = -1 * ($(this).height() / 2),
      //         left = -1 * ($(this).width() / 2);

      //       return top + 'px 0 0 ' + left + 'px';
      //     });
      //   }, 1);
      // }

      //onclick event add remove outlet
      $(document).on('click','.add-outlet',function(){
        var outlet_name2      = $('.outlet_name').val();
        var outlet_address2      = $('.outlet_address').val();
       
        var temp        = '<tr><td>'+outlet_name2+'</td><td>'+outlet_address2+'<input type="hidden" name="outlet_name2[]" value="'+outlet_name2+'"><input type="hidden" name="outlet_address2[]" value="'+outlet_address2+'"></td><td><button type="button" class="btn btn-danger delete-outlet"><i class="fa fa-trash"></i></button></td></tr>';
        $('.outlet-data').append(temp);
      })
      $(document).on('click','.delete-outlet',function(){
        $(this).closest('tr').remove();
      })
    });

    //  function initAutocomplete() {
    //     var lat = parseFloat($('#latbox').val());
    //     var lng = parseFloat($('#lngbox').val());
        
    //     markers = [];
    //     map = new google.maps.Map(document.getElementById('map'), {
    //       zoom: 12,
    //     }); 

    //     var marker = new google.maps.Marker({
    //         position: {lat: lat, lng: lng},
    //         map: map,
    //         draggable:true,
    //         zoom:12
    //     });

    //     markers.push(marker);

    //     google.maps.event.addListener(marker, 'dragstart', function(event){
    //         document.getElementById("latbox").value = event.latLng.lat();
    //         document.getElementById("lngbox").value = event.latLng.lng();
    //     });

    //     google.maps.event.addListener(marker, 'dragend', function(event){
    //         document.getElementById("latbox").value = event.latLng.lat();
    //         document.getElementById("lngbox").value = event.latLng.lng();
    //     });

    //     // Create the search box and link it to the UI element.
    //     var input = document.getElementById('pac-input');
    //     var searchBox = new google.maps.places.SearchBox(input);
    //     map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    //     // Bias the SearchBox results towards current map's viewport.
    //     map.addListener('bounds_changed', function() {
    //       searchBox.setBounds(map.getBounds());
    //     });

    //     // Listen for the event fired when the user selects a prediction and retrieve
    //     // more details for that place.
    //     searchBox.addListener('places_changed', function() {
    //       var places = searchBox.getPlaces();

    //       if (places.length == 0) {
    //         return;
    //       }

    //       // Clear out the old markers.
    //       markers.forEach(function(marker) {
    //         marker.setMap(null);
    //       });
    //       markers = [];

    //       // For each place, get the icon, name and location.
    //       var bounds = new google.maps.LatLngBounds();
    //       for(var i = 0;i < 1; i++){
    //         place = places[i];
    //         var marker = new google.maps.Marker({
    //           map: map,
    //           title: place.name,
    //           position: place.geometry.location,
    //           draggable:true,
    //           zoom:1
    //         });
    //         markers.push(marker);
    //         if (place.geometry.viewport) {
    //           // Only geocodes have viewport.
    //           bounds.union(place.geometry.viewport);
    //         } else {
    //           bounds.extend(place.geometry.location);
    //         }

    //         document.getElementById("latbox").value = place.geometry.location.lat();
    //         document.getElementById("lngbox").value = place.geometry.location.lng();

    //         google.maps.event.addListener(marker, 'dragstart', function(event){
    //             document.getElementById("latbox").value = event.latLng.lat();
    //             document.getElementById("lngbox").value = event.latLng.lng();
    //         });
    //         google.maps.event.addListener(marker, 'dragend', function(event){
    //             document.getElementById("latbox").value = event.latLng.lat();
    //             document.getElementById("lngbox").value = event.latLng.lng();
    //         });
    //       }
    //       map.fitBounds(bounds);
    //     });
    // }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpMppLOBQyYyKg11P20E9pe4Hs-cwlH9U&libraries=places&callback=initAutocomplete" async defer></script>
  <!-- croppie -->
  <script src="{{asset('components/back/js/croppie.js')}}"></script>
  <!-- <script src="{{asset('components/back/js/demo.js')}}"></script> -->
   <!-- <script>
            Demo.init();
        </script> -->
  <!-- croppie -->
@endpush
@endsection
