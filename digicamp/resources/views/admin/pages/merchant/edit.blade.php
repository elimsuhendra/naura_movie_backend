@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$merchant->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <div class="actions">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">

       
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'merchant_name','label' => 'Name','value' => (old('name') ? old('name') : $merchant->merchant_name),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

         {!!view($view_path.'.builder.text',['type' => 'email','name' => 'email','label' => 'Email','value' => (old('email') ? old('email') : $merchant->email),'attribute' => 'required','form_class' => 'col-md-6'])!!}

        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : $merchant->phone),'attribute' => 'required','form_class' => 'col-md-6'])!!}


        {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'address','label' => 'Headquarter Address','value' => (old('address') ? old('address') : $merchant->address),'attribute' => 'required','form_class' => 'col-md-6'])!!}

         {!!view($view_path.'.builder.select',['name' => 'city','label' => 'City','value' => (old('city') ? old('city') : $merchant->id_city),'attribute' => 'required','form_class' => 'col-md-6', 'data' => $data2, 'class' => 'select2'])!!}

         {!!view($view_path.'.builder.select',['name' => 'merchant_category','label' => 'Category Merchant','value' => (old('merchant_category') ? old('merchant_category') : $merchant->id_merchant_category),'attribute' => 'required','form_class' => 'col-md-6', 'data' => $data1, 'class' => 'select2'])!!}

        <div class="form-group col-md-12" style="">
          <div class="form-group col-md-2" style="">
                <div class="md-checkbox">
                  <label>Sticky</label>
                <input type="checkbox" id="checkbox_form_2" class="md-check sticky-news" name="sticky" value="y" {{isset($merchant->sticky) ? ($merchant->sticky == 'y' ? 'checked' : '') : ''}}>
                <label for="checkbox_form_2">
                  <span></span>
                      <span class="check"></span>
                      <span class="box"></span>
                </label>
              </div>
          </div>
        </div>
        
          <div class="form-group form-md-line-input col-md-12">
              <label>Logo</label><br>
              <label class="btn green input-file-label-logo">
                <input type="file" class="form-control col-md-12 single-image" name="logo"> Pilih File
              </label>
                  <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="logo">Hapus</button>
                <input type="hidden" name="remove-single-image-logo" value="n">
                <br>
              <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

              <div class="form-group single-image-logo col-md-12">
                <img src="{{asset($image_path.'/'.$merchant->logo)}}" class="img-responsive thumbnail single-image-thumbnail">
              </div>
          </div>
      </div>
       <hr/>
      <div class="row">
            <div class="form-group col-md-12">
              <label for="tag" class="sub-title">Outlet</label>
            </div>
              <input type="hidden" id="data_outlet" value="">

              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'outlet_name','label' => 'Outlet Name','value' => (old('outlet_name') ? old('outlet_name') : ''),'attribute' => '','form_class' => 'col-md-6', 'class'=>'outlet_name'])!!}

              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'outlet_email','label' => 'Outlet Email','value' => (old('outlet_email') ? old('outlet_email') : ''),'attribute' => '','form_class' => 'col-md-6', 'class'=>'outlet_email'])!!}

              {!!view($view_path.'.builder.text',['type' => 'text','name' => 'outlet_phone','label' => 'Outlet Phone','value' => (old('outlet_phone') ? old('outlet_phone') : ''),'attribute' => '','form_class' => 'col-md-6', 'class'=>'outlet_phone'])!!}

              {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'outlet_address','label' => 'Outlet Address','value' => (old('outlet_address') ? old('outlet_address') : ''),'attribute' => '','form_class' => 'col-md-6','class'=>'outlet_address'])!!}

            <div class="col-md-12">
              {!!view($view_path.'.builder.button',['type' => 'button','label' => 'Add','class' => 'add-outlet'])!!}
            </div>
          <hr/>
          <div class="table-responsive redeem-auto col-md-12">
            <table class="table table-bordered">
              <thead>
                <th>Outlet Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Address</th>
                <th>Action</th>
              </thead>
              <tbody class="outlet-data">
                @foreach($data3 as $d3)
                  <tr>                                  
                    <td>{{$d3->outlet_name}}</td>
                    <td>{{$d3->phone}}</td>
                    <td>{{$d3->email}}</td>
                    <td>{{$d3->address}}  
                      <input type="hidden" name="outlet_name2[]" value="{{$d3->outlet_name}}">
                      <input type="hidden" name="outlet_phone2[]" value="{{$d3->phone}}">
                      <input type="hidden" name="outlet_email2[]" value="{{$d3->email}}">
                      <input type="hidden" name="outlet_address2[]" value="{{$d3->address}}"></td>
                    <td><button type="button" class="btn btn-danger delete-outlet"><i class="fa fa-trash"></i></button></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
      </div>
       <hr/>
     <div class="row promo-data">
            <div class="form-group col-md-12 form-group-promo">
              <label for="tag" class="sub-title">Promo</label>
            </div>
            <input type="hidden" id="no_promo" value="{{(count($data4) > 0 ? count($data4) + 1: 1)}}">

            <div class="form-group col-md-12 header-dropdown" id="header-dropdown_0">
             <!--  <div class="sub-title-header" style="">Promo:</div> -->
              <button class="btn dropdown-toggle red-wahana show-promo" id="show-promo_0" type="button" data-toggle="dropdown" aria-expanded="false">  Promo: 0 
              <span class="caret"></span></button>
              <button type="button" class="btn btn-danger delete-outlet red-wahana add-promo" id="add-promo_0"><i class="fa fa-plus"></i></button>
            </div>

            <div class="form-group col-md-12 content-dropdown" id="content-dropdown_0" style="display: none;">
               <div class="col-md-6">
                  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control promo_name" id="promo_name_0" name="promo_name[]" value=""  placeholder="Promo Name">
                    <label for="form_floating_hg1">Promo Name <span class="" aria-required="true"></span></label>
                    <small></small>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group form-md-line-input">
                    <textarea class="form-control promo_description" id="promo_description_0" name="promo_description[]"  placeholder="Promo Description"></textarea>
                    <label for="form_floating_ZV5">Promo Description <span class="" aria-required="true"></span></label>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group form-md-line-input">
                    <textarea class="form-control term_condition editor" id="term_condition_0" name="term_condition[]"  placeholder="Term &amp; Condition"></textarea>
                    <label for="form_floating_J4T">Term &amp; Condition <span class="" aria-required="true"></span></label>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group form-md-line-input">
                    <input type="text" id="promo-start-date_0" class="form-control datepicker promo-start-date" name="start_date[]" value="" readonly="" placeholder="Valid From">
                    <label for="form_floating_Hqd">Valid From <span class="" aria-required="true">*</span></label>
                    <small></small>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group form-md-line-input">
                    <input type="text" id="promo-end-date_0" class="form-control datepicker promo-end-date" name="end_date[]" value="" readonly="" placeholder="Valid To">
                    <label for="form_floating_Hqd">Valid To <span class="" aria-required="true">*</span></label>
                    <small></small>
                  </div>
                </div>

                <div class="form-group form-md-line-input col-md-12">
                    <label>Image</label><br>
                    <label class="btn green input-file-label-image_promo">
                      <input type="hidden" name="promo_image_index[]" value="0" >
                      <input type="file" class="form-control col-md-12 single-image promo_image" name="promo_image_0"> Pilih File
                    </label>
                      <br>
                    <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>
                    <input type="hidden" value="" name="img_promo_image[]">
                    <div class="form-group single-image-promo_image_0 col-md-12">
                      <img src="{{asset($image_path2)}}/none.png" class="img-responsive thumbnail single-image-thumbnail img-promo_image_0">
                    </div>
                </div>
            </div>
            

            @if(count($data4) > 0)
                @php
                      $i = 0;
                @endphp
                @foreach($data4 as $p)
                    @php
                      $i++;
                    @endphp
                    <div class="form-group col-md-12 header-dropdown" id="header-dropdown_{{$i}}">
                     <!--  <div class="sub-title-header" style="">Promo:</div> -->
                     <input type="hidden" name="id_table_promo[]" value="{{$p->id}}">
                      <button class="btn dropdown-toggle red-wahana show-promo" id="show-promo_{{$i}}" type="button" data-toggle="dropdown" aria-expanded="false">  Promo:  {{$p->promo_name}}
                      <span class="caret"></span></button>
                    </div>

                    <div class="form-group col-md-12 content-dropdown" id="content-dropdown_{{$i}}" style="display: none;">
                       <div class="col-md-6">
                          <div class="form-group form-md-line-input">
                            <input type="text" class="form-control promo_name" id="promo_name_{{$i}}" name="promo_name[]" value="{{$p->promo_name}}" placeholder="Promo Name">
                            <label for="form_floating_hg1">Promo Name <span class="required" aria-required="true"></span></label>
                            <small></small>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group form-md-line-input">
                            <textarea class="form-control promo_description" id="promo_description_{{$i}}" name="promo_description[]" placeholder="Promo Description" value="">{{$p->description}}</textarea>
                            <label for="form_floating_ZV5">Promo Description <span class="required" aria-required="true"></span></label>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group form-md-line-input">
                            <textarea class="form-control term_condition editor" id="term_condition_{{$i}}" name="term_condition[]" placeholder="Term &amp; Condition" value="">{{$p->term_condition}}</textarea>
                            <label for="form_floating_J4T">Term &amp; Condition <span class="required" aria-required="true"></span></label>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group form-md-line-input">
                            <input type="text" id="promo-start-date_{{$i}}" class="form-control datepicker promo-start-date" name="start_date[]" value="{{date_format(date_create($p->start_date),'Y-m-d')}}" readonly="" placeholder="Valid From">
                            <label for="form_floating_Hqd">Valid From <span class="required" aria-required="true">*</span></label>
                            <small></small>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group form-md-line-input">
                            <input type="text" id="promo-end-date_{{$i}}" class="form-control datepicker promo-end-date" name="end_date[]" value="{{date_format(date_create($p->end_date),'Y-m-d')}}" required="" readonly="" placeholder="Valid To">
                            <label for="form_floating_Hqd">Valid To <span class="required" aria-required="true">*</span></label>
                            <small></small>
                          </div>
                        </div>

                      <div class="form-group form-md-line-input col-md-12">
                          <label>Image</label><br>
                          <label class="btn green input-file-label-image_promo">
                            <input type="hidden" name="promo_image_index[]" value="{{$i}}" >
                            <input type="file" class="form-control col-md-12 single-image promo_image" name="promo_image_{{$i}}"> Pilih File
                          </label>
                            <br>
                          <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>
                          <input type="hidden" value="{{$p->image}}" name="img_promo_image[]">
                          <div class="form-group single-image-promo_image_{{$i}} col-md-12">
                            <img src="{{asset($image_path3.'/'.$p->image)}}" class="img-responsive thumbnail single-image-thumbnail img-promo_image_{{$i}}">
                          </div>
                      </div>
                    </div>
                @endforeach
              @endif
      </div>
      {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      // $.tinymce();
    //   $('input,select,checkbox,button.remove-single-image').attr('disabled',true);
      $(document).on('click','.add-outlet',function(){
        var outlet_name2       = $('.outlet_name').val();
        var outlet_address2    = $('.outlet_address').val();
        var outlet_phone2      = $('.outlet_phone').val();
        var outlet_email2      = $('.outlet_email').val();

        var error       = '';
        var returnfalse =   0;
        if(outlet_name2 == ""){
            returnfalse = 1;
            error = 'Outlet Name required !';
        }else if(outlet_address2 == ""){
            returnfalse = 1;
            error = 'Outlet Address required !';
        }

        if(returnfalse == 1){
          alert(error);
        }else{
            var temp        = '<tr><td>'+outlet_name2+'</td><td>'+outlet_phone2+'</td><td>'+outlet_email2+'</td><td>'+outlet_address2+'<input type="hidden" name="outlet_name2[]" value="'+outlet_name2+'"><input type="hidden" name="outlet_address2[]" value="'+outlet_address2+'"><input type="hidden" name="outlet_phone2[]" value="'+outlet_phone2+'"><input type="hidden" name="outlet_email2[]" value="'+outlet_email2+'"></td><td><button type="button" class="btn btn-danger delete-outlet"><i class="fa fa-trash"></i></button></td></tr>';
            $('.outlet-data').append(temp);
        }
        
      });

      $(document).on('click','.add-promo',function(){ 
          var id = $(this).attr('id');
          id = id.split('_');
          id = id[1];
          var promo_name            = $('#promo_name_'+id).val();
          var promo_description     = $('#promo_description_'+id).val();
          var term_condition        = $('#term_condition_'+id).val();

          var error       = '';
          var returnfalse =   0;
          if(promo_name == ""){
            error = "Promo Name required !";
            returnfalse =   1;
          }else if(term_condition == ""){
            error = "Promo Term & Condition required !";
            returnfalse =   1;
          }
          // alert(returnfalse);
          if(returnfalse == 1){
            alert(error);
          }else{
            var i = $("#no_promo").val();

            var form =  '<div class="form-group col-md-12 header-dropdown" id="header-dropdown_'+i+'">'+
                          '<button class="btn dropdown-toggle red-wahana show-promo" id="show-promo_'+i+'" type="button" data-toggle="dropdown" aria-expanded="false">  Promo: '+i+
                          '<span class="caret"></span></button>&nbsp;'+
                          // '<button type="button" class="btn btn-danger red-wahana delete-promo" id="delete-promo_'+i+'"><i class="fa fa-minus"></i></button>'+
                        '</div>'+

                        '<div class="form-group col-md-12 content-dropdown" id="content-dropdown_'+i+'" style="display: none;">'+

                          '<div class="col-md-6">'+
                            '<div class="form-group form-md-line-input">'+
                              '<input type="text" id="form_floating_hg1" class="form-control promo_name" id="promo_name_'+i+'" name="promo_name[]" value="" placeholder="Promo Name">'+
                              '<label for="form_floating_hg1">Promo Name <span class="required" aria-required="true"></span></label>'+
                              '<small></small>'+
                            '</div>'+
                          '</div>'+

                          '<div class="col-md-12">'+
                            '<div class="form-group form-md-line-input">'+
                              '<textarea class="form-control promo_description" id="promo_description_'+i+'" name="promo_description[]" placeholder="Promo Description"></textarea>'+
                              '<label for="form_floating_ZV5">Promo Description <span class="required" aria-required="true"></span></label>'+
                            '</div>'+
                          '</div>'+

                          '<div class="col-md-12">'+
                            '<div class="form-group form-md-line-input">'+
                              '<textarea class="form-control term_condition editor" id="term_condition_'+i+'" name="term_condition[]" placeholder="Term &amp; Condition"></textarea>'+
                              '<label for="form_floating_J4T">Term &amp; Condition <span class="required" aria-required="true"></span></label>'+
                            '</div>'+
                          '</div>'+

                          '<div class="col-md-6">'+
                            '<div class="form-group form-md-line-input">'+
                              '<input type="text" id="promo-start-date_'+i+'" class="form-control datepicker promo-start-date" name="start_date[]" value="" readonly="" placeholder="Valid From">'+
                              '<label for="form_floating_Hqd">Valid From <span class="required" aria-required="true">*</span></label>'+
                              '<small></small>'+
                            '</div>'+
                          '</div>'+

                          '<div class="col-md-6">'+
                            '<div class="form-group form-md-line-input">'+
                              '<input type="text" id="promo-end-date_'+i+'" class="form-control datepicker promo-end-date" name="end_date[]" value="" readonly="" placeholder="Valid To">'+
                              '<label for="form_floating_Hqd">Valid To <span class="required" aria-required="true">*</span></label>'+
                              '<small></small>'+
                            '</div>'+
                          '</div>'+


                          '<div class="form-group form-md-line-input col-md-12">'+
                            '<label>Image</label><br>'+
                            '<label class="btn green input-file-label-image_promo">'+
                              '<input type="hidden" name="promo_image_index[]" value="'+i+'" >'+
                              '<input type="file" class="form-control col-md-12 single-image promo_image_'+i+'" name="promo_image_'+i+'"> Pilih File'+
                            '</label>'+
                              '<br>'+

                            '<small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>'+
                            '<div class="form-group single-image-promo_image_'+i+' col-md-12">'+
                              '<input type="hidden" value="" name="img_promo_image[]">'+
                              '<img src="{{asset($image_path2)}}/none.png" class="img-responsive thumbnail single-image-thumbnail img-promo_image_'+i+'">'+
                            '</div>'+
                        '</div>';
            $('.promo-data').append(form);
            i++;
            $("#no_promo").val(i);
            $.datepickers();
            $.tinymce();
          }
      });

       $(document).on('click','.show-promo',function(){
          var id = $(this).attr('id');
          id = id.split("_");
          id = id[1];
          $('#content-dropdown_'+id).toggle();
          // alert(id);
       }); 

        $(document).on('click','.delete-promo',function(){
          var id = $(this).attr('id');
          id = id.split("_");
          id = id[1];
          // alert(id);
          $('#header-dropdown_'+id).remove();
          $('#content-dropdown_'+id).remove();
        })

      // $(document).on('click','.add-promo',function(){
      //   var data1      = $('.promo_name').val();
      //   var data2      = $('.promo_descripton').val();
      //   var data3      = $('.term_condition').val();
      //   var src        = $('.img-promo_image').attr("src");
      //   var img        = "<img src='"+src+"' class='img-responsive thumbnail single-image-thumbnail'>";
      //   var file       = $(".promo_image")[0].files;


      //   var temp        = '<tr><td>'+data1+'</td><td>'+data2+'</td><td>'+data3+'</td><td>'+file+'<input type="hidden" name="promo_name2[]" value="'+data1+'"><input type="hidden" name="promo_descripton2[]" value="'+promo_descripton2+'"><input type="hidden" name="term_condition2[]" value="'+term_condition2+'"><input type="hidden" name="term_condition2[]" value="'+term_condition2+'"></td><td><button type="button" class="btn btn-danger delete-outlet"><i class="fa fa-trash"></i></button></td></tr>';
      //   $('.outlet-data').append(temp);
      // });

      $(document).on('click','.delete-outlet',function(){
        $(this).closest('tr').remove();
      })
    });
  </script>
@endpush
@endsection
