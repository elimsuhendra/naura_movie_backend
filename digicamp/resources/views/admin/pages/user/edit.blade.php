@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/{{$user->id}}" enctype="multipart/form-data">
	{{ method_field('PUT') }}
	<div class="portlet light bordered">
    	<div class="portlet-title">
			<div class="caption font-green">
				<i class="icon-layers font-green title-icon"></i>
				<span class="caption-subject bold uppercase"> {{$title}}</span>
			</div>
			<div class="actions">
				<a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
			</div>
    	</div>
    	<div class="portlet-body form">
      		@include('admin.includes.errors')
  			<div class="row">
				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'username','label' => 'Username','value' => (old('username') ? old('username') : $user->username),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

				<div class="col-md-12">
					<div class="form-group form-md-line-input" hre="">
						<label for="form_floating_hre">Password <span class="required" aria-required="true">*</span></label>
				 		<div class="input-group">
				      		<input type="password" class="form-control password" id="password" name="password" placeholder="Password" value="">
				      		<span class="input-group-addon">
	                            <a id="show-password" class="text-default"><i class="fa fa-eye"></i></a>
	                        </span>

	                        <span class="input-group-btn">
	                            <a id="generate-password" class="btn btn-primary">Generate</a>
	                        </span>
				    	</div>
					
						<small></small>

					</div>
				</div>

				{!!view($view_path.'.builder.text',['type' => 'email','name' => 'email','label' => 'Email','value' => (old('email') ? old('email') : $user->email),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'name','label' => 'Name','value' => (old('name') ? old('name') : $user->name),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : $user->phone),'attribute' => 'autofocus','form_class' => 'col-md-12', 'required' => 'n'])!!}

				{!!view($view_path.'.builder.select',['name' => 'user_access_id','label' => 'User Access','value' => (old('user_access_id') ? old('user_access_id') : $user->user_access_id),'attribute' => 'required','form_class' => 'col-md-12', 'data' => $user_access, 'class' => 'select2', 'onchange' => ''])!!}

			  	<!-- {!!view($view_path.'.builder.radio',['name' => 'status','label' => 'Status','value' => (old('status') ? old('status') : $user->status),'attribute' => 'required','form_class' => 'col-md-12', 'data' => ['y' => 'Active', 'n' => 'Not-active'], 'class' => 'select2'])!!} -->
			  	<!-- <div class="form-group col-md-2"style={{in_array(auth()->guard($guard)->user()->user_access_id,[1,9]) ? '' : "display:none;"}}>
				  	<div class="md-checkbox">
				  		<label>Login Web</label>
						<input type="checkbox" id="checkbox_form_1" class="md-check login_web" name="login_web" {{isset($user->login_web) ? ($user->login_web == 'y' ? 'checked' : '') : ''}} value="y" >
						<label for="checkbox_form_1">
							<span></span>
					        <span class="check"></span>
					        <span class="box"></span>
						</label>
					</div>
				</div>

				<div class="form-group col-md-2" style={{in_array(auth()->guard($guard)->user()->user_access_id,[1,9]) ? '' : "display:none;"}}>
					<div class="md-checkbox">
				  		<label>Login App</label>
						<input type="checkbox" id="checkbox_form_2" class="md-check login_app" name="login_app" {{isset($user->login_app) ? ($user->login_app == 'y' ? 'checked' : '') : ''}} value="y">
						<label for="checkbox_form_2">
							<span></span>
					        <span class="check"></span>
					        <span class="box"></span>
						</label>
					</div>
				</div> -->
				
			  	{!!view($view_path.'.builder.file',['name' => 'picture','label' => 'Picture','value' => $user->picture,'type' => 'file','file_opt' => ['path' => $image_path],'upload_type' => 'single-image','class' => 'col-md-12','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 138 x 44 px','form_class' => 'col-md-12', 'required' => 'n'])!!}

  				<input type="hidden" id="root-url" value="{{$path}}" />
  				<input type="hidden" id="store_id" value="{{$user->store_id}}" />
			</div>	
			{!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
		</div>
	</div>
</form>
@push('custom_scripts')
	<script>
		$(document).ready(function(){
			
		});
	</script>
@endpush
@endsection