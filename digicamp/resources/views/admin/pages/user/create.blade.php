@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
	<div class="portlet light bordered">
    	<div class="portlet-title">
			<div class="caption font-green">
				<i class="icon-layers font-green title-icon"></i>
				<span class="caption-subject bold uppercase"> {{$title}}</span>
			</div>
			<div class="actions">
				<a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
			</div>
    	</div>
    	<div class="portlet-body form">
      		@include('admin.includes.errors')
  			<div class="row">
  				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'name','label' => 'Name','value' => (old('name') ? old('name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-12', 'class' => 'name'])!!}

				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'username','label' => 'Username','value' => (old('username') ? old('username') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-12', 'class' => 'username'])!!}				

				<div class="col-md-12">
					<div class="form-group form-md-line-input" hre="">
						<label for="form_floating_hre">Password <span class="required" aria-required="true">*</span></label>
				 		<div class="input-group">
				      		<input type="password" class="form-control password" id="password" name="password" placeholder="Password">
				      		<span class="input-group-addon">
	                            <a id="show-password" class="text-default"><i class="fa fa-eye"></i></a>
	                        </span>

	                        <span class="input-group-btn">
	                            <a id="generate-password" class="btn btn-primary">Generate</a>
	                        </span>
				    	</div>
					
						<small></small>

					</div>
				</div>

				{!!view($view_path.'.builder.text',['type' => 'email','name' => 'email','label' => 'Email','value' => (old('email') ? old('email') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-12'])!!}

				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-12', 'required' => 'n'])!!}

				{!!view($view_path.'.builder.select',['name' => 'user_access_id','label' => 'User Access','value' => (old('user_access_id') ? old('user_access_id') : '1'),'attribute' => 'required','form_class' => 'col-md-12', 'data' => $user_access, 'class' => 'select2', 'onchange' => ''])!!}

		         <!--    <div class="form-group col-md-2" style={{in_array(auth()->guard($guard)->user()->user_access_id,[1,12]) ? '' : "display:none;"}}>
					  	<div class="md-checkbox">
					  		<label>Login Web</label>
							<input type="checkbox" id="checkbox_form_1" class="md-check login_web" name="login_web" value="y" >
							<label for="checkbox_form_1">
								<span></span>
						        <span class="check"></span>
						        <span class="box"></span>
							</label>
						</div>
					</div>


				<div class="form-group col-md-2" style={{in_array(auth()->guard($guard)->user()->user_access_id,[1,12]) ? '' : "display:none;"}}>
					<div class="md-checkbox">
				  		<label>Login App</label>
						<input type="checkbox" id="checkbox_form_2" class="md-check login_app" name="login_app"  value="y">
						<label for="checkbox_form_2">
							<span></span>
					        <span class="check"></span>
					        <span class="box"></span>
						</label>
					</div>
				</div> -->

			  	{!!view($view_path.'.builder.file',['name' => 'picture','label' => 'Picture','value' => '','type' => 'file','file_opt' => ['path' => 'components/admin/images/user/'],'upload_type' => 'single-image','class' => 'col-md-12','note' => 'Note: File Must jpeg,png,jpg,gif | Best Resolution: 138 x 44 px','form_class' => 'col-md-12', 'required' => 'n'])!!}

  				<input type="hidden" id="root-url" value="{{$path}}" />
			</div>	
			{!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
		</div>
	</div>
</form>
@push('custom_scripts')
	<script>
		$(document).ready(function(){
			var password = $(".password");
			var username = $(".username");
			var name = $(".name");
			
			$('#generate-password').on('click', function(e){
	        	var randomstring = Math.random().toString(36).slice(-6);
		        password.val(randomstring);
		    });

		    $('#show-password').on('click', function(e){
		        if(password.attr("type") == "password"){
		            password.attr("type", "text");
		            $("#show-password").addClass("text-primary");
		            $("#show-password").removeClass("text-default");
		        }
		        else{
		            password.attr("type", "password");
		            $("#show-password").addClass("text-default");
		            $("#show-password").removeClass("text-primary");
		        }
		    });

		    $(document).on('blur','.name',function(e){
		    	var randomstring = Math.random().toString(36).slice(-3);
		    	var username2 = name.val() + randomstring;
				username.val(username2);
 			});
		});
	</script>
@endpush
@endsection