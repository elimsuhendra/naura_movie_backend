@extends($view_path.'.layouts.master')
@push('css')
  <link href="{{asset('components/back/css/pages/profile-2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="profile">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> Overview </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled profile-nav">
                            <li>
                                @if($data1->images != null && file_exists($image_path.'/'.$data1->images))
                                    <img src="{{asset($image_path.'/'.$data1->images)}}" class="img-responsive pic-bordered" alt="" />
                                @else
                                    <img src="{{asset('components/admin/image/default.jpg')}}" class="img-responsive pic-bordered" alt="" />
                                @endif
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-8 profile-info">
                                <h4 class="font-green sbold">{{$data1->name}}</h4>
                                <ul class="list-inline">
                                    <li>
                                        <i class="fa fa-envelope"></i> {{$data1->email}} </li>
                                    <li>
                                        <i class="fa fa-phone"></i> {{$data1->phone}} </li>
                                    <li>
                                        <i class="fa {{$data1->gender == 'm' ? 'fa-male' : 'fa-female'}}"></i> {{$data1->gender == "m" ? "Male" : "Female"}} </li>
                                    <li>
                                        <i class="fa fa-birthday-cake"></i> {{date('d M Y', strtotime($data1->birth_date))}} </li>
                                </ul>

                                <ul class="list-inline">
                                    <li>
                                        <label class="label label-default">Waiting payment - {{count($data1->preorder()->where('preorder_status', 1)->get())}}</label></li>
                                    <li>
                                        <label class="label label-info">Payment confirmation - {{count($data1->preorder()->where('preorder_status', 2)->get())}}</label></li>
                                    <li>
                                        <label class="label label-primary">Ready for redeem - {{count($data1->preorder()->where('preorder_status', 3)->get())}}</label></li>
                                    <li>
                                        <label class="label label-success">Redeemed - {{count($data1->preorder()->where('preorder_status', 4)->get())}}</label></li>
                                    <li>
                                        <label class="label label-danger">Cancel - {{count($data1->preorder()->where('preorder_status', 11)->get())}}</label></li>
                                </ul>
                            </div>
                            <!--end col-md-8-->
                            <div class="col-md-4">
                                
                            </div>
                            <!--end col-md-4-->
                        </div>
                        <!--end row-->
                        <div class="tabbable-line tabbable-custom-profile">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_11" data-toggle="tab"> Order History </a>
                                </li>
                                <!-- <li>
                                    <a href="#tab_1_12" data-toggle="tab"> Aktivitas </a>
                                </li> -->
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_11">
                                    <div class="portlet-body">
                                        @if(count($data1->preorder) > 0)
                                        {{ $data1->preorder()->paginate(10)->links() }}    
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                <thead>
                                                    <tr>
														<th>
														  <i class="fa fa-building"></i> Cinema </th>
														<th>
														  <i class="fa fa-television"></i> Type </th>
														<th>
														  <i class="fa fa-clock-o"></i> Showtime </th>
														<th>
														  <i class="fa fa-ticket"></i> Qty </th>
														<th>
														  <i class="fa fa-tags"></i> Amount </th>
														<th>
														    <i class="fa fa-info"></i> Status </th>
                                                      </tr>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                	@foreach($data1->preorder()->paginate(10) as $po)
                                                		<tr>
                                                			<td>
                                                				{{$po->cinema_tickets->cinema->bioskop_name}}
                                                			</td>
                                                			<td>
                                                				{{$po->cinema_tickets->cinema_service->name}}
                                                			</td>
                                                			<td>
                                                				{{date('d M Y', strtotime($po->cinema_tickets->cinema_date))}} {{$po->cinema_tickets->cinema_time}}
                                                			</td>
                                                			<td>
                                                				{{number_format($po->total_ticket)}}
                                                			</td>
                                                			<td>
                                                				{{number_format($po->price)}}
                                                			</td>
                                                			<td>
                                                				@if($po->preorder_status == 1)
                                                					<label class="label label-default">Waiting payment</label>
                                                				@elseif($po->preorder_status == 2)
                                                					<label class="label label-info">Payment confirmation</label>
                                                				@elseif($po->preorder_status == 3)
                                                					<label class="label label-primary">Ready for redeem</label>
                                                				@elseif($po->preorder_status == 4)
                                                					<label class="label label-success">Redeemed</label>
                                                				@elseif($po->preorder_status == 11)
                                                					<label class="label label-danger">Cancel</label>
                                                				@endif
                                                			</td>
                                                		</tr>
                                                	@endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        @else
                                          <div class="alert alert-warning">No Data</div>
                                        @endif
                                    </div>
                                </div>

                                <!-- <div class="tab-pane" id="tab_1_12">
                                    <div class="portlet-body">
                                      
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end tab-pane-->
        </div>
    </div>
</div>
@endsection
@push('custom_scripts')

@endpush