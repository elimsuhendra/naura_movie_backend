@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
	<div class="portlet light bordered">
    	<div class="portlet-title">
			<div class="caption font-green">
				<i class="icon-layers font-green title-icon"></i>
				<span class="caption-subject bold uppercase"> {{$title}}</span>
			</div>
			<div class="actions">
				<a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
			</div>
    	</div>
    	<div class="portlet-body form">
      		@include('admin.includes.errors')
  			<div class="row">
  				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'name','label' => 'Name','value' => (old('name') ? old('name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'name'])!!}

  				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'email','label' => 'Email','value' => (old('email') ? old('email') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'email'])!!}

  				<div class="col-md-12">
					<div class="form-group form-md-line-input" hre="">
						<label for="form_floating_hre">Password <span class="required" aria-required="true">*</span></label>
				 		<div class="input-group">
				      		<input type="password" class="form-control password" id="password" name="password" placeholder="Password">
				      		<span class="input-group-addon">
	                            <a id="show-password" class="text-default"><i class="fa fa-eye"></i></a>
	                        </span>

	                        <span class="input-group-btn">
	                            <a id="generate-password" class="btn btn-primary">Generate</a>
	                        </span>
				    	</div>
					
						<small></small>

					</div>
				</div>	

				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'phone','label' => 'Phone','value' => (old('phone') ? old('phone') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => '', 'placeholder' => ''])!!}

				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'ttl','label' => 'Place, Date Birth','value' => (old('ttl') ? old('ttl') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => '', 'placeholder' => 'Jakarta, 01-10-1992'])!!}	

				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'no_machine','label' => 'No. Machine','value' => (old('no_machine') ? old('no_machine') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'no_card','label' => 'No. Card','value' => (old('no_card') ? old('no_card') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}		

				<!-- {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'address','label' => 'Address','value' => (old('address') ? old('address') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'address'])!!} -->
	

				<div class="form-group form-md-line-input col-md-12">
		            <label>Photo</label><br>
		            <label class="btn green input-file-label-photo">
		              <input type="file" class="form-control col-md-12 single-image" name="photo"> Pilih File
		            </label>
		                <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="photo">Hapus</button>
		              <input type="hidden" name="remove-single-image-photo" value="n">
		              <br>
		            <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

		            <div class="form-group single-image-photo col-md-12">
		              <img src="{{asset($image_path2)}}/none.png" class="img-responsive thumbnail single-image-thumbnail">
		            </div>
		        </div>

  				<input type="hidden" id="root-url" value="{{$path}}" />
			</div>	
			{!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
		</div>
	</div>
</form>
@push('custom_scripts')
	<script>
		$(document).ready(function(){
			var password = $(".password");
			// var username = $(".username");
			var name = $(".name");
 			// $(document).on('change','.select-merchant',function(res){
				// getStore('');
 			// });

			// function getStore(id){	
			// 	var merchant_id = $('.select-merchant option:selected').val();
			// 	$(".select-store").children('option:not(:first)').remove();	
			// 	$("#select2-store_id-eb-container").text("");		
			//  	var url    = $.root() + 'users/user/get_store/' + merchant_id;
			//  	console.log(url);
			//     $.ajax({
			// 		url: url,
			// 		type: "GET",
			// 		dataType: 'json',
			// 		// data: par
	  //         	}).done(function(msg) {
	  //         		var option='';
			// 		$.each(msg, function( index, value ) {
			// 			if(id){
			// 				var selected = (id == index) ? 'selected' : '';
			// 			}
			// 			option += "<option value="+index+" "+selected+">"+value+"</option>";
			// 		});
			// 		$(".select-store").append(option);
			// 		var selected_id = $(".select-store option:selected").val();
			// 		// if(selected_id){
			// 		// 	var selected_text = $(".select-store option:selected").text();
			// 		// 	// $(".select2-selection__rendered").text(selected_text);
			// 		// }
	  //         	});
			
			//    	// return data;
			// }

			
			$('#generate-password').on('click', function(e){
	        	var randomstring = Math.random().toString(36).slice(-6);
		        password.val(randomstring);
		    });

		    $('#show-password').on('click', function(e){
		        if(password.attr("type") == "password"){
		            password.attr("type", "text");
		            $("#show-password").addClass("text-primary");
		            $("#show-password").removeClass("text-default");
		        }
		        else{
		            password.attr("type", "password");
		            $("#show-password").addClass("text-default");
		            $("#show-password").removeClass("text-primary");
		        }
		    });

		  //   $(document).on('blur','.name',function(e){
		  //   	var randomstring = Math.random().toString(36).slice(-3);
		  //   	var username2 = name.val() + randomstring;
				// username.val(username2);
 			// });
		});
	</script>
@endpush
@endsection