@extends($view_path.'.layouts.master')
@push('css')
  <style>
    .table-form .form-control{
      border-bottom: none !important;
      height: initial !important;
      padding: initial !important;
    }

    .table-form .form-group.form-md-line-input{
      margin-bottom: 0 !important;
    }

    .cinema_col{
      min-width: 305px;
    }
  </style>
@endpush
@section('content')
<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">
        {!!view($view_path.'.builder.text',['type' => 'text','name' => 'ticket_name','label' => 'Name','value' => (old('ticket_name') ? old('ticket_name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}

        <div class="clearfix"></div>

        {!!view($view_path.'.builder.select',['id' => 'cinema_type','name' => 'cinema_type','label' => 'Cinema Type','value' => (old('type_option') ? old('type_option') : '1'),'attribute' => 'required','form_class' => 'col-md-4', 'data' => $cinema_service, 'class' => 'select2', 'onchange' => ''])!!}

        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <label class="control-label">Cinema Time</label>
            <div class="input-group date form_datetime">
                <input type="text" size="16" readonly class="form-control" id="cinema_time">
                <span class="input-group-btn">
                    <button class="btn default date-set" type="button">
                        <i class="fa fa-calendar"></i>
                    </button>
                </span>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group form-md-line-input">
            <label class="control-label">&nbsp;</label>
            <div class="input-group">
                <input type="button" class="btn btn-success" onclick="addSchedule()" value="Add">
            </div>
          </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12">
          <div class="table-scrollable">
            <table id="schedule" class="table table-bordered table-form">
              <thead>
                <tr class="parent">
                  <td rowspan="2" class="cinema_col" align="center"><b>Cinema</b></td>
                </tr>
              </thead>

              <tbody>
                @foreach($cinema as $c)
                  <tr id="cinema_{{$c->id}}" valign="middle">
                    <td>{{$c->bioskop_name}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>

        <div class="col-md-12 actions margin-top-20">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
        </div>
    </div>
  </div>
</form>
@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      $(".form_datetime").datetimepicker({
          autoclose: true,
          isRTL: App.isRTL(),
          format: "dd/mm/yyyy - hh:ii",
          pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
          minuteStep: 1
      });
    });

    function addSchedule(){
      var cinemaTime = $("#cinema_time").val();
      var cinemaType = $("#cinema_type").select2('data');

      if(cinemaTime == ""){
        alert("Cinema time is required")
      }
      else{
        if($('#date-'+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")).length){
          var colspan = parseInt($('#date-'+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")).attr('colspan'));
          var nextColspan = parseInt($('#date-'+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")).attr('colspan')) + 1;
          $('#date-'+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")).attr('colspan', nextColspan);

          var append =  "<td align='center' class='time-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+"'>"+cinemaTime.split(" - ")[1]+"</td>";

          $(append).insertAfter('td.time-'+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+":last");

          @foreach($cinema as $c)
            var append2 = "<td class='qtywrapper-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+"'>"+
              "<div class='form-group form-md-line-input'>"+
               "<input name='qty[]' class='form-control text-center'>"+
               "<input type='hidden' name='cinema[]' value='{{$c->id}}'>"+
               "<input type='hidden' name='showtime[]' value='"+cinemaTime+"'>"+
               "<input type='hidden' name='service[]' value='"+cinemaType[0].id+"'>"+
              "</div>"+
            "</td>";

            $(append2).insertAfter("#schedule tr#cinema_{{$c->id}} td.qtywrapper-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+":last");
          @endforeach
        }
        else{
          var append3 = "<td align='center' id='date-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+"' colspan='1'>"+
            cinemaTime.split(" - ")[0]+" ("+cinemaType[0].text+")</td>";

          $("#schedule thead tr.parent").append(append3);

          if($(".parent2").length){
            var append = "<td align='center' class='time-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+"'>"+cinemaTime.split(" - ")[1]+"</td>";

            $("#schedule thead tr.parent2").append(append);
          }
          else{
            var append =
                "<tr class='parent2'>"+
                  "<td align='center' class='time-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+"'>"+cinemaTime.split(" - ")[1]+"</td>"+
                "</tr>";

            $("#schedule thead").append(append);
          }

          @foreach($cinema as $c)
            var append2 = "<td class='qtywrapper-"+cinemaTime.split(" - ")[0].replace(/[\/]/g, "")+"'>"+
              "<div class='form-group form-md-line-input'>"+
               "<input name='qty[]' class='form-control text-center'>"+
               "<input type='hidden' name='cinema[]' value='{{$c->id}}'>"+
               "<input type='hidden' name='showtime[]' value='"+cinemaTime+"'>"+
               "<input type='hidden' name='service[]' value='"+cinemaType[0].id+"'>"+
              "</div>"+
            "</td>";

            $("#schedule tr#cinema_{{$c->id}}").append(append2);
          @endforeach
        }
      }
    }

  </script>
@endpush
@endsection
