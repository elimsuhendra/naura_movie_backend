@extends($view_path.'.layouts.master')
@section('content')
  <style>
      /*.swiper-container {
          width: 100%;
          height: 100%;
      }
      .swiper-slide {
          text-align: center;
          font-size: 18px;
          background: #fff;
          
          display: -webkit-box;
          display: -ms-flexbox;
          display: -webkit-flex;
          display: flex;
          -webkit-box-pack: center;
          -ms-flex-pack: center;
          -webkit-justify-content: center;
          justify-content: center;
          -webkit-box-align: center;
          -ms-flex-align: center;
          -webkit-align-items: center;
          align-items: center;
      }*/

      .col {
		  /*background: #ccc;*/
		  height: 250px;
		}
		.row > .red {
		  background: red;
		  color: #fff;
		}
		.row .blue {
		  background: blue;
		  color: #fff;
		}
		.row .green {
		  background: green;
		}
		.row .orange {
		  background: orange;
		}
		.row > .comments-site-feed {
		  /*background: black;*/
		  color: #fff;
		  height: 500px;
		}
  </style>
<div class="content-push">
	    <!-- Slider main container -->
		<div class="swiper-container">
		    <!-- Additional required wrapper -->
		    <div class="swiper-wrapper">
		        <!-- Slides -->
		        @foreach($slide_show as $ss)
		        	<div class="swiper-slide">
		        		<div class="label-top-left2">{!! $ss->description !!}</div>
		        		<img src="{{asset('components/admin/image/slideshow/'.$ss->image)}}" width="100%"/>
		        	</div>
		        @endforeach
		    </div>
		    <!-- If we need pagination -->
		    <div class="swiper-pagination swiper-pagination-index" style="bottom: 40px !important; text-align: center;left: 0px !important;"></div>
		    
		    <!-- If we need navigation buttons -->
		    <div class="swiper-button-prev"></div>
		    <div class="swiper-button-next"></div>
		    
		    <!-- If we need scrollbar -->
		    <div class="swiper-scrollbar"></div>
		    <div class="label-bottom-right btn-slider"><button class="button btn-klik-daftar"><a href="{{url('/register')}}" style="color:#fff;">KLIK DISINI UNTUK DAFTAR</button></div>
		</div>

	<div class="row information-blocks">
				<div class="col-xs-6 col-sm-4">
					<div class="why-us">
						<div class="why-us-title">{{$alasan_1_name->value}}</div>
						<div class="why-us-description">{{$alasan_1->value}}</div>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4">
					<div class="why-us">
						<div class="why-us-title">{{$alasan_2_name->value}}</div>
						<div class="why-us-description">{{$alasan_2->value}}</div>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4">
					<div class="why-us">
						<div class="why-us-title">{{$alasan_3_name->value}}</div>
						<div class="why-us-description">{{$alasan_3->value}}</div>
					</div>
				</div>
	</div>

	<div class="row merchant-content">
		<div class="header-merchant-content col-sm-12 col-xs-12">MERCHANTS</div>
		<div class="row" style="padding: 0px 24px;">
			<div class="col-sm-5 comments-site-feed padding-sticky" style="padding: 5px 5px;">
				<a href="{{url('/merchants_category/'.$headline_merchant_category->id_merchant_category)}}">
					<img src="{{asset('components/admin/image/merchant_category/'.$headline_merchant_category->image)}}" width="100%" height="100%"/>
					<div class="label-center"><span style="padding: 10px;background-color: #191919;font-family: arial;font-weight: bold;font-size: 18px;color: #fff;">{{strtoupper($headline_merchant_category->category_name)}}</span></div>
				</a>
			</div>
			@if(count($headline_merchant_category2) >0)
				@php
					$i = 0;
				echo '<div class="col-sm-7" style="padding: 0px 5px;">';
					foreach($headline_merchant_category2 as $sm){
						$i++;
							echo '<div class="col-sm-4 padding-sticky col"><a href="'.url('/merchants_category/'.$sm->id_merchant_category).'"><img src="'.asset('components/admin/image/merchant_category/'.$sm->image).'" width="100%" height="100%"/></a><div class="label-bottom-right"><span style="
							    background-color: white;
							    padding: 10px 20px;
							    font-family: arial;
							    font-size: 16px;
							    font-weight: bold;
							    color: #292D2F;
							">'.$sm->category_name.'</span></div></div>';
					}

					if(count($headline_merchant_category2) < 6){
						$sisa = 6-count($headline_merchant_category2);
						for($j = 0; $j<$sisa; $j++){
							echo '<div class="col-sm-4 padding-sticky col"><a href="'.url('/merchants_category/'.$sm->id_merchant_category).'"></a><img src="'.asset('components/both/images/web/none.png').'" width="100%" height="100%"/></div>';
						}
					}
				echo '</div>';
				@endphp
			@endif
		</div>

		<div class="row merchant-newest">
			@php
				if(count($sticky_merchant) > 0){
					foreach($sticky_merchant as $m){
						echo'<div class="col-sm-4">
							<a href="'.url('/merchants/'.$m->id).'"><img src="'.asset('components/admin/image/merchant/'.$m->logo).'" width="100%" height="100%"/></a>
							<div class="merchant-detail">
								<div class="merchant-title">'.$m->merchant_name.'</div>
								<div class="merchant-city">'.$m->city_name.'</div>
							</div>
						</div>';

						if(count($sticky_merchant) < 3){
							$sisa = 3-count($sticky_merchant);
							for($j = 0; $j<$sisa; $j++){
								echo'<a href="'.url('/merchants/'.$m->id).'"></a><img src="'.asset('components/both/images/web/none.png').'" width="100%" height="100%"/>';
							}
						}
					}
				}
			@endphp
		</div>
		<div class="row content-view-all">
			<button class="button btn-default btn-view-all form-control" data-id=""><a href="{{url('/merchants')}}" style="color: #fff;">View All</a></button>
		</div>
	</div>

  	<div class="row merchant-content">
		<div class="header-merchant-content col-sm-12 col-xs-12">News</div>
		<div class="row" style="padding: 0px 24px;">
			<div class="col-sm-5 comments-site-feed padding-sticky" style="padding: 5px 5px;">
				<a href="{{url('/news/'.$headline_news->id)}}">
					<img src="{{asset('components/admin/image/news/'.$headline_news->image)}}" width="100%" height="88%"/>
					<div class="label-bottom-left2"><span style="padding: 10px 10px;background-color: #fff;font-family: arial;font-weight: bold;font-size: 29px;color: black;">{{$headline_news->title}}</span></div>
					<div class="news-desc">
						@php
							$content = preg_replace("/<img[^>]+\>/i", "", $headline_news->description);
						@endphp
						<span style="
						        background-color: white;
							    font-family: arial;
							    font-size: 18px;
							    color: #808080;
						">{!! substr($content, 0, 100) !!}<a href="{{url('/news/'.$sm->id)}}"> more >></a>
						</span>
					</div>
				</a>
			</div>
			@if(count($news) > 0)
				@php
					$i = 0;
				echo '<div class="col-sm-7" style="padding: 0px 5px;">';
					foreach($news as $sm){
						$i++;
						$content = preg_replace("/<img[^>]+\>/i", "", $sm->description);
							echo 	'<div class="col-sm-4 padding-sticky col">
										<img src="'.asset('components/admin/image/news/'.$sm->image).'" width="100%" height="75%" />
										<div class="news-desc">
											<span style="
											        background-color: white;
												    font-family: arial;
												    font-size: 18px;
												    color: #808080;
											">'.substr($content, 0, 100).'<a href="'.url('/news/'.$sm->id).'"> more >></a>
											</span>
										</div>
									</div>';
					}

					if(count($news) < 6){
						$sisa = 6-count($news);
						for($j = 0; $j<$sisa; $j++){
							echo '<div class="col-sm-4 padding-sticky col"><a href="'.url('/news/'.$sm->id).'"><img src="'.asset('components/both/images/web/none.png').'" width="100%" height="88%"/></a></div>';
						}
					}
				echo '</div>';
				@endphp
			@endif
		</div>

		<div class="row merchant-newest">
			@php
				if(count($sticky_news) > 0){
					foreach($sticky_news as $m){
	
						$content = preg_replace("/<img[^>]+\>/i", "", $m->description);
					
						echo'<div class="col-sm-4">
							<a href="'.url('/news/'.$m->id).'"></a><img src="'.asset('components/admin/image/news/'.$m->image).'" width="100%" height="100%"/>
							<div class="merchant-detail">
								<div class="merchant-title">'.$m->title.'</div>
								<div class="merchant-city">'.$content.'</div>
							</div>
						</div>';

						if(count($sticky_merchant) < 3){
							$sisa = 3-count($sticky_merchant);
							for($j = 0; $j<$sisa; $j++){
								echo'<a href="'.url('/merchants/'.$m->id).'"></a><img src="'.asset('components/both/images/web/none.png').'" width="100%" height="100%"/>';
							}
						}
					}
				}
			@endphp
		</div>
		<div class="row content-view-all">
			<button class="button btn-default btn-view-all form-control" data-id=""><a href="{{url('/news')}}" style="color: #fff;">View All</a></button>
		</div>
	</div>
<hr/>
<div class="clear"></div>
@endsection

@push('custom_scripts')
<script>
	var swiper = new Swiper('.swiper-container', {
	    pagination: '.swiper-pagination',
	    paginationClickable: true,
	    nextButton: '.swiper-button-next',
	    prevButton: '.swiper-button-prev',
	});
</script>
@endpush
