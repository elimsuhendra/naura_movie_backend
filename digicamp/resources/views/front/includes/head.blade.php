<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="format-detection" content="telephone=no" />
<meta http-equiv="Content-Language" content="id">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
<title>Wahana</title>

<meta content="Wahana" name="keywords" />
<meta content="Website Wahana" name="description" />
<meta name="geo.placename" content="Indonesia">
<meta name="geo.country" content="ID">
<meta name="language" content="Indonesian">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="root_url" content="{{url($root_path)}}/" />

<!-- Core CSS -->
<link rel ="stylesheet" href="{{asset('components/plugins/bootstrap/css/bootstrap.min.css')}}">
<link rel ="stylesheet" href="{{asset('components/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<link rel ="stylesheet" href="{{asset('components/front/css/idangerous.swiper.css')}}">
<link rel ="stylesheet" href="{{asset('components/plugins/swiper-slider/swiper.min.css')}}">
<link rel ="stylesheet" href="{{asset('components/front/css/style.css')}}">
<link rel ="stylesheet" href="{{asset('components/front/css/custom_style.css')}}">
<link rel ="stylesheet" href="{{asset('components/front/css/app.css')}}">
