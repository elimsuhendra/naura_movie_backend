<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Ticket;
use digipos\models\Cinema;
use digipos\models\Cinema_service;
use digipos\models\Cinema_tickets;
use digipos\models\Config;

// use Request;
use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use digipos\Libraries\Email;
use Illuminate\Http\Request;

class TicketController extends KyubiController{
	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard);
		$this->middleware($this->role_guard);
		$this->primary_field	= 'id';
		$this->title			= 'Ticket';
		$this->root_link		= 'Ticket';
		$this->bulk_action_data = [1];
		$this->model			= new Ticket;
		$this->bulk_action		= true;
		$this->image_path 		= 'components/both/images/movie/';
		$this->data['image_path'] = $this->image_path;
	}

	public function index(){
		$this->field = [
			[
				'name' 		=> 'ticket_name',
				'label' 	=> 'Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];

		$this->model = $this->model;
		return $this->build('index');
	}

	public function create(){
		$this->data['title'] = "Create Ticket";

		$this->data['cinema'] = Cinema::get();
		$this->data['cinema_service'] = $this->build_array(Cinema_service::get(),'id','name');

		return $this->render_view('pages.ticket.create');
	}

	public function store(Request $request){
		$this->validate($request,[
				'ticket_name' => 'required',
			]);

		$config = Config::where("name", "film_name")->first();
		$config->value = $request->ticket_name;
		$config->save();
		
		$cinema = $request->cinema;
		$service = $request->service;
		$showtime = $request->showtime;
		$qty = $request->qty;

		foreach($cinema as $key => $c){
			$ct = new Cinema_tickets;
			$ct->cinema_id = $c;
			$ct->cinema_service_id = $service[$key];
			$ct->total_ticket = $qty[$key];
			$ct->stock = $qty[$key];
			$ct->hold = 0;
			$ct->sold = 0;
			$ct->cinema_date = $this->dtpToSql(explode(" - ", $showtime[$key])[0]);
			$ct->cinema_time = explode(" - ", $showtime[$key])[1].":00";
			$ct->save();
		}

		Alert::success('Successfully create ticket');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->data['ticket'] = Ticket::find(1);
		$this->data['cinema_tickets'] = Cinema_tickets::join('cinema', 'cinema.id', '=', 'cinema_tickets.cinema_id')
			->orderBy('bioskop_name', 'asc')
			->orderBy('cinema_date', 'asc')
			->orderBy('cinema_time', 'asc')
			->paginate(10);
		$this->data['title'] = "View Ticket";

		return $this->render_view('pages.ticket.view');
	}

	public function edit($id){
		$this->data['title'] = "Edit Ticket";

		$this->data['cinema'] = Cinema::orderBy('bioskop_name', 'asc')->get();
		$this->data['cinema_service'] = $this->build_array(Cinema_service::get(),'id','name');

		$this->data['ticket'] = Ticket::find(1);
		$this->data['cinema_date'] = Cinema_tickets::orderBy('cinema_date', 'asc')
			->orderBy('cinema_service_id', 'asc')
			->orderBy('cinema_time', 'asc')
			->where('cinema_id', 1)
			->groupBy('cinema_date')
			->groupBy('cinema_service_id')
			->get();
		$this->data['cinema_tickets'] = Cinema_tickets::orderBy('cinema_date', 'asc')
			->orderBy('cinema_service_id', 'asc')
			->orderBy('cinema_time', 'asc')
			->where('cinema_id', 1)
			->get();

		$this->data['tickets'] = Cinema_tickets::join('cinema', 'cinema.id', '=', 'cinema_tickets.cinema_id')
			->orderBy('bioskop_name', 'asc')
			->orderBy('cinema_date', 'asc')
			->orderBy('cinema_service_id', 'asc')
			->orderBy('cinema_time', 'asc')
			->get();

		return $this->render_view('pages.ticket.edit');
	}

	public function update(Request $request, $id){
		$this->validate($request,[
				'ticket_name' => 'required',
			]);

		$ticket = Ticket::find($id);
		$ticket->ticket_name = $request->ticket_name;
		$ticket->synopsis = $request->synopsis;

		if($request->input('remove-single-image-image') == 'y'){
			if($ticket->image != null){
				File::delete($this->image_path.$ticket->image);
				$ticket->image = '';
			}
		}
		if ($request->hasFile('image')){
        	// File::delete($this->image_path.$this->model->picture);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$ticket->image = $image;
		}

		$ticket->save();

		$ct = Cinema_tickets::truncate();
		
		$cinema = $request->cinema;
		$service = $request->service;
		$showtime = $request->showtime;
		$qty = $request->qty;

		foreach($cinema as $key => $c){
			$ct = new Cinema_tickets;
			$ct->cinema_id = $c;
			$ct->cinema_service_id = $service[$key];
			$ct->total_ticket = $qty[$key];
			$ct->stock = $qty[$key];
			$ct->hold = 0;
			$ct->sold = 0;
			$ct->cinema_date = $this->dtpToSql(explode(" - ", $showtime[$key])[0]);
			$ct->cinema_time = explode(" - ", $showtime[$key])[1].":00";
			$ct->save();
		}

		Alert::success('Successfully edit ticket');
		return redirect()->to($this->data['path']);
	}									

	public function destroy(Request $request){
		// return $this->build('delete');

		$id = $request->id;
		$uc = $this->model->find($id);
		$store = Store::where('merchant_id', $uc->id);
		$user = $this->user->where('merchant_id', $uc->id);
		$uc->delete();
		$user->delete();
		$store->delete();
		Alert::success('Merchant has been deleted');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function export(){
		$store = $this->myStore();

		$list_member_id = Member::whereIn('store_id', $store)->groupBy('user_id')->pluck('user_id')->toArray();
		return $this->build_export($list_member_id);
	}
}
?>