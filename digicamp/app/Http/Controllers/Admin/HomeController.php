<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Config;
use digipos\models\Social_media;
use digipos\models\Home;
use digipos\models\Slideshow;
use digipos\models\Banner;

use digipos\Libraries\Alert;
// use Request;
use Illuminate\Http\Request;
use File;
use Cache;
use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Email;
use Carbon\Carbon;

class HomeController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->model 		 	= new Home;
		$this->social_media 	= new Social_media;
		$this->slideshow 		= new Slideshow;
		$this->banner 			= new Banner;
		$this->data['title']	= trans('general.general-settings');
		$this->data['image_path'] = 'components/front/images/footer/';
		$this->data['image_path2'] = 'components/both/images/web/';
		$this->data['image_path3'] = 'components/admin/image/slideshow/';
		$this->data['image_path4'] = 'components/admin/image/banner/';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$config = $this->model->get();
		foreach($config as $c){
			$configs[$c->name] = $c->value;
		}
		$configs = (object)$configs;
		$this->data['home'] = $configs;

		$this->data['slideshow'] 	= $this->slideshow->get();
		$this->data['banner'] 		= $this->banner->get();
		// $social_media = $this->social_media->get();
		// foreach($social_media as $sm){
		// 	$social_medias[$sm->name] = $sm->value;
		// }
		// $social_medias = (object)$social_medias;
		// $this->data['social_media'] = $social_medias;
		return $this->render_view('pages.home.index');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request){
		$all 	=  $request->all();
		foreach($all as $r => $qr){
			$q = $this->model->where('name',$r)->first();
			if ($q){
				if($q->value != $qr){
					if ($request->hasFile($r)){
						$data = [
								'name' => $r,
								'file_opt' => ['path' => $this->data['image_path']],
								'old_image'	=> $this->model->value
							];
						$image = $this->build_image($data);
						$qr = $image;
					}
					$q->value = $qr;
					$q->save();
				}
			}
		}

		$slideshow 	= $this->slideshow->get();
		$banner 		= $this->banner->get();
		foreach($slideshow as $s){
			$q = $this->slideshow->where('id',$s->id)->first();
			if($request->input('remove-single-image-slideshow_'.$s->id) == 'y'){
				if($q->image != NULL){
					File::delete($this->data['image_path3'].$q->image);
					$qr = '';
					$q->image = $qr;
				}
			}

			if($request->hasFile('slideshow_'.$s->id)){
				$this->validate($request,[
					'slideshow_'.$s->id 			=> 'mimes:jpeg,png,jpg,gif',
				]);

				$data = [
						'name' => 'slideshow_'.$s->id,
						'file_opt' => ['path' => $this->data['image_path3']]
					];
				$image = $this->build_image($data);
				$qr = $image;
				$q->image = $qr;
			}

			$q->description = $request->input('slideshow_description_'.$s->id);
			
			// var_dump($q);
			$q->save();
		}
		// dd('test');
		foreach($banner as $s){
			$q = $this->banner->where('id',$s->id)->first();
			if($request->input('remove-single-image-banner_'.$s->id) == 'y'){
				if($q->image != NULL){
					File::delete($this->data['image_path4'].$q->image);
					$qr = '';
					$q->image = $qr;
				}
			}

			if ($request->hasFile('banner_'.$s->id)){
				// var_dump('banner_'.$s->id);
				$data = [
						'name' => 'banner_'.$s->id,
						'file_opt' => ['path' => $this->data['image_path4']]
					];
				$image = $this->build_image($data);
				var_dump('test');
				$qr = $image;
				$q->image = $qr;
			}
			$q->description = $request->input('banner_description_'.$s->id);
			$q->save();

		}
		
		Alert::success('Successfully change '.$this->data['title']);
		return redirect()->back();
	}
}
