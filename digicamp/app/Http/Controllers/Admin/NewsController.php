<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Msmerchant;
use digipos\models\Merchant_category;
use digipos\models\Authmenu;
use digipos\models\Useraccess;
use digipos\models\Store;
use digipos\models\User;
use digipos\models\News;
use digipos\models\Config;
// use Request;
use Validator;
use Auth;
use Hash;
use DB;
use File;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;

class NewsController extends KyubiController{
	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard);
		$this->middleware($this->role_guard);
		$this->primary_field	= 'id';
		$this->title			= 'News';
		$this->root_link		= 'news';
		$this->model			= new News;
		$this->user				= new user;
		// $this->delete_relation	= ['store'];
		$this->bulk_action		= true;
		$this->bulk_action_data = [3];
		$this->image_path 		= 'components/admin/image/news/';
		$this->data['image_path'] 	= $this->image_path;
		$this->data['image_path2'] 	= 'components/both/images/web/';

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	public function index(){
		$this->field = [
			[
				'name' => 'image',
				'label' => 'Image',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path]
			],
			[
				'name' 		=> 'title',
				'label' 	=> 'Title',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'created_at',
				'label' 	=> 'Created At',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			],
		];
		$this->model = $this->model;
		return $this->build('index');
	}

	public function field_create(){
		$field = [
			[
				'name' => 'title',
				'label' => 'Titlte',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general',
			],
			[
				'name' => 'description',
				'label' => 'Description',
				'type' => 'textarea',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'image',
				'label' => 'Image',
				'type' => 'file',
				'file_opt' => ['path' => $this->image_path],
				'upload_type' => 'single-image',
				'form_class' => 'col-md-6 pad-left',
				'validation' => 'mimes:jpeg,png,jpg,gif|max:2000',
				'note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb'
			]
		];
		return $field;
	}

	public function field_edit(){
		$field = [
			[
				'name' => 'title',
				'label' => 'Titlte',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general',
			],
			[
				'name' => 'description',
				'label' => 'Description',
				'type' => 'textarea',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'image',
				'label' => 'Image',
				'type' => 'file',
				'file_opt' => ['path' => $this->image_path],
				'upload_type' => 'single-image',
				'form_class' => 'col-md-6 pad-left',
				'validation' => 'mimes:jpeg,png,jpg,gif|max:2000',
				'note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb',
				'value' => $this->model->image
			]
		];
		return $field;
	}

	public function create(){
		// $this->field = $this->field_create();
		// return $this->build('create');

		$this->data['title'] = "Create News";
		return $this->render_view('pages.news.create');
	}

	public function store(Request $request){
		$this->validate($request,[
				'title' 		=> 'required|min:5',
				'description' 	=> 'required',
				'image'			=> 'mimes:jpeg,png,jpg,gif',
				'slug'			=> 'required|unique:news,slug',
			]);

		$this->model->title					= $request->title;
		$this->model->description			= $request->description;
		$this->model->status 				= 'y';
		$this->model->meta_title 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_title->value;
		$this->model->meta_description 		= $request->meta_title != NULL ? $request->meta_description : $this->meta_description->value;

		$this->model->meta_keyword 			= $request->meta_title != NULL ? $request->meta_keyword : $this->meta_keyword->value;

		$this->model->updated_by 			= auth()->guard($this->guard)->user()->id;
		$this->model->slug 					= $request->slug;
		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->image = $image;
		}
		// var_dump($request->headline_news.'-'.$request->sticky_news);
		($request->headline_news == 'y' ? $this->model->headline_news = 'y' : $this->model->headline_news = 'n');
		($request->sticky_news == 'y' ? $this->model->sticky_news = 'y' : $this->model->sticky_news = 'n');
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully add new news');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->data['merchant'] = 	DB::table('msmerchant as a')
									->join('user as b','a.id','=','b.merchant_id')
									->where('a.id',$id)
									->select('a.*','b.email','b.user_access_id')
									->first();
		$this->data['user_access'] = $this->get_user_access();
		$this->data["title"] = "View Merchant ".$this->data['merchant']->merchant_name;
		return $this->render_view('pages.merchant.view');
	}

	public function edit($id){
		// $this->model = $this->model->find($id);
		// $this->field = $this->field_edit();
		// return $this->build('edit');

		$this->data['title'] = "Edit News";
		$this->data['data1'] = $this->model->find($id);
		// dd($this->data['data1']);
		return $this->render_view('pages.news.edit');
	}

	public function update(Request $request, $id){
		$this->validate($request,[
				'title' 		=> 'required|min:5',
				'description' 	=> 'required',
				'image'			=> 'mimes:jpeg,png,jpg,gif',
				'slug'			=> 'required|unique:news,slug,'.$id,
			]);

		$this->model = $this->model->find($id);
		$this->model->title					= $request->title;
		$this->model->description			= $request->description;
		$this->model->status 				= 'y';
		$this->model->meta_title 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_title->value;
		$this->model->meta_description 		= $request->meta_title != NULL ? $request->meta_description : $this->meta_description->value;

		$this->model->meta_keyword 			= $request->meta_title != NULL ? $request->meta_keyword : $this->meta_keyword->value;


		$this->model->updated_by 			= auth()->guard($this->guard)->user()->id;
		$this->model->slug 					= $request->slug;

		if($request->input('remove-single-image-image') == 'y'){
			if($this->model->image != NULL){
				File::delete($this->image_path.$this->model->image);
				$this->model->image = '';
			}
		}

		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->image = $image;
		}	
		// var_dump($request->headline_news.' - '.$request->sticky_news);
		($request->headline_news == 'y' ? $this->model->headline_news = 'y' : $this->model->headline_news = 'n');
		($request->sticky_news == 'y' ? $this->model->sticky_news = 'y' : $this->model->sticky_news = 'n');

		$this->model->updated_at = date("Y-m-d H:i:s");
		$this->model->save();

		Alert::success('Successfully edit new news');
		return redirect()->to($this->data['path']);
	}							

	public function destroy(Request $request){
		// return $this->build('delete');

		$id = $request->id;
		$uc = $this->model->find($id);
		// $store = Store::where('merchant_id', $uc->id);
		// $user = $this->user->where('merchant_id', $uc->id);

		$uc->delete();
		// $uc->deleted_at = date("Y-m-d H:i:s");
		// $uc->save();
		Alert::success('News has been deleted');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function export(){
		return $this->build_export();
	}

	public function sorting(){
		$this->field = [
			[
				'name' 		=> 'merchant_name',
				'label' 	=> 'Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text',
				'type' 		=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->model = $this->model->where('id','>','1');
		return $this->build('sorting');
	}

	public function dosorting(){
		return $this->dosorting();
	}
}
?>