<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Authmenu;
use digipos\models\Useraccess;
use digipos\models\User;
use digipos\models\Cinema;
use digipos\models\Cinema_service;
use digipos\models\Cinema_price;
use digipos\models\Province;
use digipos\models\City;
use digipos\models\Config;

// use Request;
use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;
use Carbon\Carbon;
use File;


class CinemaController extends KyubiController{
	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard);
		$this->middleware($this->role_guard);
		$this->primary_field	= 'id';
		$this->title			= 'Cinema';
		$this->root_link		= 'cinema';
		$this->model			= new Cinema;
		$this->user				= new User;
		$this->province			= new Province;
		$this->city				= new City;
		$this->cinema_service   = new Cinema_service;
		$this->cinema_price   	= new Cinema_price;
		// $this->delete_relation	= ['store'];
		$this->bulk_action		= true;
		$this->bulk_action_data = [1];
		$this->image_path 		= 'components/both/images/cinema/';
		$this->image_path3		= 'components/both/images/cinema_service/';
		$this->data['image_path'] 	= $this->image_path;
		$this->data['image_path2'] 	= 'components/both/images/web/';
		$this->data['image_path3'] 	= $this->image_path3;

		$this->days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thrusday', 'Friday', 'Saturday'];
		$this->data['days'] = $this->days;

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	public function index(){
		$this->field = [
			[
				'name' 		=> 'cinema_code',
				'label' 	=> 'Cinema Code',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'bioskop_name',
				'label' 	=> 'Bioskop Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'province_name',
				'label' 	=> 'Provinsi',
				'sorting' 	=> 'n',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'city_name',
				'label' 	=> 'City',
				'sorting' 	=> 'n',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->model = $this->model
						->select('cinema.*', 'province.name as province_name', 'city.name as city_name')
						->join('province', 'province.id', '=', 'cinema.province_id')
						->join('city', 'city.id', '=', 'cinema.city_id');

		return $this->build('index');
	}

	public function field_create(){
		$field = [
			[
				'name' => 'merchant_name',
				'label' => 'Name',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general',
			],
			[
				'name' => 'address',
				'label' => 'Address',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'email',
				'label' => 'Email',
				'type' => 'email',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'phone',
				'label' => 'Phone',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
		];
		return $field;
	}

	public function field_edit(){
		$field = [
			[
				'name' => 'merchant_name',
				'label' => 'Name',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'owner',
				'label' => 'Owner',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'email',
				'label' => 'Email',
				'type' => 'email',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general'
			],
			[
				'name' => 'user_access_id',
				'label' => 'User Access',
				'type' => 'select',
				'attribute' => 'required',
				'validation' => 'required',
				// 'data' => $this->get_user_access(),
				'class' => 'select2'
			]
		];
		return $field;
	}

	public function create(){
		// $this->field = $this->field_create();
		// return $this->build('create');
		$this->data['title'] = "Create Store";
		$this->data['data1'] = $this->province->pluck('name', 'id')->toArray();
		$this->data['data2'] = json_encode($this->city->get());
		// $this->data['cinema_service'] = $this->cinema_service->get();
		// dd($this->data['cinema_service']);
		return $this->render_view('pages.cinema.create');
	}

	public function store(Request $request){
		$this->validate($request,[
				'bioskop_name' 	=> 'required|unique:cinema,bioskop_name',
				'cinema_code' 	=> 'required|unique:cinema,cinema_code',
				'address' 		=> 'required',
		]);

		$this->model->bioskop_name			= $request->bioskop_name;
		$this->model->cinema_code			= $request->cinema_code;
		$this->model->phone					= $request->phone;
		$this->model->address				= $request->address;
		$this->model->description			= $request->description;
		$this->model->province_id			= $request->province;
		$this->model->city_id				= $request->city;
		$this->model->status 				= 'y';
		// $this->model->meta_title 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_title->value;
		// $this->model->meta_description 		= $request->meta_title != NULL ? $request->meta_title : $this->meta_description->value;

		// $this->model->meta_keyword 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_keyword->value;

		$this->model->upd_by 				= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully add new Cinema');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->data['cinema'] 	= DB::table('cinema')->find($id);
		$this->data['data1'] 	= $this->province->pluck('name', 'id')->toArray();
		$this->data['data2'] 	= json_encode($this->city->get());
		$this->data['cinema_service'] = $this->cinema_service->get();
		$this->data['cinema_price'] = $this->cinema_price->where('cinema_id', $id)->get();

		$this->data["title"] = "View Cinema ".$this->data['cinema']->bioskop_name;
		return $this->render_view('pages.cinema.view');
	}

	public function edit($id){
		$this->data['cinema'] 	= DB::table('cinema')->find($id);
		$this->data['data1'] 	= $this->province->pluck('name', 'id')->toArray();
		$this->data['data2'] 	= json_encode($this->city->get());
		$this->data['cinema_service'] = $this->cinema_service->get();
		$this->data['cinema_price'] = $this->cinema_price->where('cinema_id', $id)->get();

		$this->data["title"] = "Edit Cinema ".$this->data['cinema']->bioskop_name;

		return $this->render_view('pages.cinema.edit');
	}

	public function update(Request $request, $id){
		$this->validate($request,[
				'bioskop_name' 	=> 'required|unique:cinema,bioskop_name,'.$id,
				'cinema_code' 	=> 'required|unique:cinema,cinema_code,'.$id,
				'address' 		=> 'required',
		]);
		$this->model						= $this->model->find($id);
		$this->model->bioskop_name			= $request->bioskop_name;
		$this->model->cinema_code			= $request->cinema_code;
		$this->model->phone					= $request->phone;
		$this->model->address				= $request->address;
		$this->model->description			= $request->description;
		$this->model->province_id			= $request->province;
		$this->model->city_id				= $request->city;
		$this->model->status 				= 'y';

		// $this->model->meta_title 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_title->value;
		// $this->model->meta_description 		= $request->meta_title != NULL ? $request->meta_title : $this->meta_description->value;

		// $this->model->meta_keyword 			= $request->meta_title != NULL ? $request->meta_title : $this->meta_keyword->value;

		$this->model->upd_by 			= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		//insert cinema_service price based on day
		$this->cinema_service = $this->cinema_service->where('status', 'y')->get();

		$temp_arr = [];
		foreach ($this->days as $key => $dy) {
			foreach ($this->cinema_service as $key2 => $cs) {
				$cinema_service = $request->input('cinema-service-'.$cs->id);
				// var_dump('cinema_service_id: '.$cs['id']);
				// var_dump(' - cinema_day: '.($key + 1));
				// var_dump('- price: '.$cinema_service[$key].'<br>');
				if(isset($cinema_service[$key]) && $cinema_service[$key] != 'NaN' && $cinema_service[$key] != false){
					// if(is_nan($cinema_service[$key])){
						$temp_arr[]			= [
							'cinema_id'			=> $this->model->id,
							'cinema_service_id'	=> $cs['id'],
							'cinema_day'		=> $key + 1,
							'price'				=> $this->decode_rupiah($cinema_service[$key]),
							'upd_by'		=> auth()->guard($this->guard)->user()->id,
							'created_at'		=> Carbon::now(),
							'updated_at'		=> Carbon::now()

						];
					// }
				}
			}
		}

		// dd($temp_arr);
		if(count($temp_arr) > 0){
			$del = Cinema_price::where('cinema_id', $id)->delete();
			Cinema_price::insert($temp_arr);
		}

		Alert::success('Successfully add create new merchant');
		return redirect()->to($this->data['path']);
	}							

	public function destroy(Request $request){
		// return $this->build('delete');

		$id = $request->id;
		$uc = $this->model->find($id);
		
		$uc->delete();
		Alert::success('Cinema has been deleted');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function export(){
		return $this->build_export();
	}

	public function sorting(){
		$this->field = [
			[
				'name' 		=> 'merchant_name',
				'label' 	=> 'Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text',
				'type' 		=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->model = $this->model->where('id','>','1');
		return $this->build('sorting');
	}

	public function dosorting(){
		return $this->dosorting();
	}

	public function get_merchant_category($par){
		$data = $this->merchant_category
			->join('msmerchant','msmerchant.id_merchant_category','=','merchant_category.id')
			->join('city','city.id','=','msmerchant.id_city')
			->join('merchant_log','merchant_log.merchant_id','=','msmerchant.id')
			->where('merchant_log.created_at','>=',DB::raw('DATE_SUB(CURDATE(), INTERVAL '.$par.' DAY)'))
			->where('merchant_log.created_at','<=',DB::raw('NOW()')); 
		$merchant_category 	= $data
							->select('merchant_category.*', 'msmerchant.merchant_name', 'msmerchant.id_merchant_category','city.name as city_name','merchant_log.ip_address', 'merchant_log.created_at', DB::raw('COUNT(*) as visits'))
							->groupBy('msmerchant.id_merchant_category')
							->get();
		return json_encode($merchant_category);
	}

	public function get_merchant_location($par){
		$merchant_location 	= $this->merchant_category
							->select('msmerchant.merchant_name', 'msmerchant.id_merchant_category', 'msmerchant.id_city','city.name as city_name','merchant_log.ip_address', 'merchant_log.created_at', DB::raw('COUNT(*) as visits'))
							->join('msmerchant','msmerchant.id_merchant_category','=','merchant_category.id')
							->join('city','city.id','=','msmerchant.id_city')
							->join('merchant_log','merchant_log.merchant_id','=','msmerchant.id')
							->where('merchant_log.created_at','>=',DB::raw('DATE_SUB(CURDATE(), INTERVAL '.$par.' DAY)'))
							->where('merchant_log.created_at','<=',DB::raw('NOW()'))
							->orderBy('msmerchant.id_city')
							->groupBy('city.id', 'msmerchant.id_city')
							->get();
		return json_encode($merchant_location);
	}
}
?>