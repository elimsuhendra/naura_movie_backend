<?php namespace digipos\Http\Controllers\Admin;

use DB;
use Session;
use Hash;
use File;

use digipos\models\User;
use digipos\models\Msmenu;
use digipos\models\Useraccess;
use digipos\models\Mslanguage;
use digipos\models\Msmerchant;
use digipos\models\Customer;

use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CustomerController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Customer";
		$this->root_url			= "users/customer";
		$this->primary_field 	= "name";
		$this->root_link 		= "customer";
		$this->model 			= new Customer;
		// $this->restrict_id 		= [1];
		$this->bulk_action 		= true;
		$this->bulk_action_data = [3];
		$this->image_path 		= 'components/admin/image/customer/';
		$this->image_path2 		= 'components/both/images/web/';
		$this->data['image_path'] 	= $this->image_path;
		$this->data['image_path2'] 	= $this->image_path2;

		$this->data['root_url']		= $this->root_url;
		// $this->data['title']	= $this->title;

		// $this->data['authmenux'] = Session('authmenux'); 
		// $this->data['msmenu'] = Session('msmenu');
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->field = [
			[
				'name' => 'photo',
				'label' => 'Photo',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path]
			],
			[
				'name' => 'name',
				'label' => 'Name',
				'sorting' => 'y',
				'search' => 'text'
			],[
				'name' => 'email',
				'label' => 'Email',
				'sorting' => 'y',
				'search' => 'text'
			],
			[
				'name' => 'status',
				'label' => 'Status',
				'type' => 'check',
				'data' => ['y' => 'Active','n' => 'Not Active'],
				'tab' => 'general'
			]
		];

		return $this->build('index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function field_create(){
		$field = [
					[
						'name' => 'username',
						'label' => 'Username',
						'type' => 'text',
						'attribute' => 'required autofocus',
						'validation' => 'required',
						'not_same' => 'y',
					],
					[
						'name' => 'email',
						'label' => 'Email',
						'type' => 'email',
						'attribute' => 'required autofocus',
						'validation' => 'email|required',
						'not_same' => 'y',
					],[
						'name' => 'password',
						'label' => 'Password',
						'type' => 'password',
						'attribute' => 'required',
						'validation' => 'required',
						'hash' => 'y'
					],[
						'name' => 'name',
						'label' => 'Name',
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'phone',
						'label' => 'Phone',
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'user_access_id',
						'label' => 'User Access',
						'type' => 'select',
						'data' => $this->get_user_access(),
						'class'	=> 'select2',
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'merchant_id',
						'label' => 'Merchant',
						'type' => 'select',
						'data' => $this->get_merchant(),
						'class'	=> 'select-merchant',
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'store_id',
						'label' => 'User Access',
						'type' => 'select',
						'data' => [],
						'class'	=> 'select2',
						'id'	=> 'store',
						'attribute' => 'required',
						'validation' => 'required',
						'disable'	=>	'true' 
					],
					[
						'name' => 'picture',
						'label' => 'Your Picture',
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif|max:2000',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb'
					],
					// [
					// 	'name' => 'status',
					// 	'label' => 'Status User',
					// 	'type' => 'radio',
					// 	'data' => ['y' => 'Active','n' => 'Not Active'],
					// 	'attribute' => 'required',
					// 	'validation' => 'required'
					// ]
				];
		return $field;
	}

	public function field_edit(){
		$field = [
					[
						'name' => 'username',
						'label' => 'Username',
						'type' => 'text',
						'attribute' => 'required autofocus',
						'validation' => 'required',
						'not_same' => 'y',
					],
					[
						'name' => 'email',
						'label' => 'Email',
						'type' => 'email',
						'attribute' => 'required autofocus',
						'validation' => 'email|required',
						'not_same' => 'y',
					],[
						'name' => 'password',
						'label' => 'Password',
						'type' => 'password',
						'attribute' => 'required',
						'validation' => 'required',
						'hash' => 'y'
					],[
						'name' => 'name',
						'label' => 'Name',
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'phone',
						'label' => 'Phone',
						'type' => 'text',
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'user_access_id',
						'label' => 'User Access',
						'type' => 'select',
						'data' => $this->get_user_access(),
						'class'	=> 'select2',
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'merchant_id',
						'label' => 'Merchant',
						'type' => 'select',
						'data' => $this->get_merchant(),
						'class'	=> 'select-merchant',
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'store_id',
						'label' => 'Store',
						'type' => 'select',
						'data' => $this->get_store(''),
						'class'	=> 'select2',
						'id'	=> 'store',
						'attribute' => 'required',
						'validation' => 'required',
						'disable'	=>	'true' 
					],
					[
						'name' => 'status',
						'label' => 'Status User',
						'type' => 'radio',
						'data' => ['y' => 'Active','n' => 'Not Active'],
						'attribute' => 'required',
						'validation' => 'required'
					],
					[
						'name' => 'picture',
						'label' => 'Your Picture',
						'type' => 'file',
						'file_opt' => ['path' => $this->image_path],
						'upload_type' => 'single-image',
						'form_class' => 'col-md-6 pad-left',
						'validation' => 'mimes:jpeg,png,jpg,gif|max:2000',
						'note' => 'Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb'
					],
				];
		return $field;
	}

	public function create(){
		$this->data['title'] 	= 'Create New '.$this->title;
		return $this->render_view('pages.customer.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request){
		$this->validate($request,[
			'name'					=> 'required',
			'email'					=> 'required|unique:customer',
			'password'				=> 'required',
			'phone'					=> 'required',
		]);
		
		$this->model->name 				= $request->name;
		$this->model->email 			= $request->email;
		$this->model->password 			= Hash::make($request->password);

		$this->model->phone 		= $request->phone;
		// $this->model->address 	= $request->address;
		$this->model->birth_date 	= $request->ttl;
		// $this->model->no_machine 	= $request->no_machine;
		// $this->model->no_card 		= $request->no_card;
		$this->model->status 		= 'y';
		
		if ($request->hasFile('photo')){
        	// File::delete($this->image_path.$this->model->picture);
			$data = [
						'name' => 'photo',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}
		if($request->input('remove-single-image-photo') == 'y'){
			File::delete($this->image_path.$this->model->photo);
			$this->model->images = '';
		}

		$this->model->status 			= 'y';
		$this->model->upd_by		= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		

		Alert::success('Successfully create customer');
		return redirect()->to($this->data['path']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		$this->data['data1'] 	= $this->model->find($id);
		// dd($this->data['data1']);
		$this->data['title'] 	= 'View New '.$this->title;
		return $this->render_view('pages.customer.view');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$this->data['data1'] 	= $this->model->find($id);
		// dd($this->data['data1']);
		$this->data['title'] 	= 'Edit New '.$this->title;
		return $this->render_view('pages.customer.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id){
		$this->validate($request,[
			'name'					=> 'required',
			'email'					=> 'required|unique:customer,email,'.$id,
			'password'				=> 'required',
			// 'phone'					=> 'required',
		]);
		
		$this->model 				= $this->model->find($id);
		$this->model->name 				= $request->name;
		$this->model->email 			= $request->email;
		$this->model->password 			= Hash::make($request->password);

		$this->model->phone 		= $request->phone;
		// $this->model->address 	= $request->address;
		$this->model->birth_date 	= Carbon::parse($request->birth_date)->format('Y-d-m');
		$this->model->gender 		= $request->gender;
		$this->model->status 		= 'y';
		
		if ($request->hasFile('photo')){
        	// File::delete($this->image_path.$this->model->picture);
			$data = [
						'name' => 'photo',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}

		if($request->input('remove-single-image-photo') == 'y'){
			if($this->model->images != NULL){
				// dd($this->image_path.$this->model->photo);
				File::delete($this->image_path.$this->model->photo);
				$this->model->images = '';
			}
		}

		$this->model->status 			= 'y';
		$this->model->upd_by		= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		

		Alert::success('Successfully create customer');
		return redirect()->to($this->data['path']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request){
		$id = $request->id;
		$uc = $this->model->find($id);
		$uc->delete();
		Alert::success('Customer has been deleted');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}


	public function export(){
		if(in_array(auth()->guard($this->guard)->user()->store_id,["0","1"])){
			$users = '';
		}else{
			$users = $this->get_userId_byStore();
		}
		return $this->build_export($users);
	}
}
