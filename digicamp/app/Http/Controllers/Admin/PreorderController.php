<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Preorder;
use digipos\models\Ticket;
use digipos\models\Push_notification;

// use Request;
use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use digipos\Libraries\Email;
use digipos\Libraries\Pushnotification;
use Illuminate\Http\Request;

class PreorderController extends KyubiController{
	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard);
		$this->middleware($this->role_guard);
		$this->primary_field	= 'id';
		$this->title			= 'Preorder';
		$this->root_link		= 'preorder';
		$this->model			= new Preorder;
		$this->bulk_action		= false;
		$this->image_path 		= 'components/both/images/movie/';
		$this->data['image_path'] = $this->image_path;
	}

	public function index(){
		$this->field = [
			[
				'name' 		=> 'name',
				'label' 	=> 'Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'preorder_date',
				'label' 	=> 'Preorder Time',
				'before_show' => ['date_format' => ['format' => 'd-M-Y<\b\\r>H:i:s']],
				'sorting' 	=> 'y',
			],
			[
				'name' 		=> 'total_ticket',
				'label' 	=> 'Qty',
				'before_show' => ['number_format' => ['decimal' => '0']],
				'sorting' 	=> 'y',
			],
			[
				'name' 		=> 'bioskop_name',
				'label' 	=> 'Cinema',
				'search' 	=> 'text',
				'sorting' 	=> 'y',
			],
			[
				'name' 		=> 'showtime',
				'label' 	=> 'Showtime',
				'before_show' => ['date_format' => ['format' => 'd-M-Y<\b\\r>H:i:s']],
				'sorting' 	=> 'y',
			],
			[
				'name' 		=> 'preorder_status',
				'belongtoraw' => ['field' => 'pstatus'],
				'after_show' => ['label' => ['Waiting Payment' => 'default', 'Payment Confirmation' => 'info', 'Ready for redeem' => 'primary', 'Redeemed' => 'success', 'Cancel' => 'danger'], 'onclick' => ['function' => 'updateStatus', 'data' => 'id', 'include' => ['Payment Confirmation', 'Ready for redeem']]],
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['1' => 'Waiting Payment', '2' => 'Payment Confirmation', '3' => 'Ready for redeem', '4' => 'Redeemed', '11' => 'Cancel'],
			]
		];

		$this->model = $this->model->select('preorder.*', 'cinema.bioskop_name', DB::raw('CONCAT(pon_cinema_tickets.cinema_date, " ", pon_cinema_tickets.cinema_time) AS showtime'), DB::raw('(CASE WHEN pon_preorder.preorder_status = "1" THEN "Waiting Payment" WHEN pon_preorder.preorder_status = "2" THEN "Payment Confirmation" WHEN pon_preorder.preorder_status = "3" THEN "Ready for redeem" WHEN pon_preorder.preorder_status = "4" THEN "Redeemed" WHEN pon_preorder.preorder_status = "11" THEN "Cancel" END) AS pstatus'))
			->join('cinema_tickets', 'cinema_tickets.id', '=', 'preorder.cinema_ticket_id')
			->join('cinema', 'cinema.id', '=', 'cinema_tickets.cinema_id');
		return $this->build('index');
	}

	public function show($id){
		$this->data['preorder'] = $this->model->find($id);
		$this->data['ticket'] = Ticket::find(1);
		
		$this->data['title'] = "View Preorder";

		return $this->render_view('pages.preorder.view');
	}			

	public function ext(Request $request, $action){
		return $this->$action($request);
	}

	public function cancel_preorder($request){
		return "success";
	}

	public function update_preorder($request){
		$po = $this->model->find($request->id);

		if($po->preorder_status == "2" || $po->preorder_status == "3"){
			$pstatus = ['1' => 'Waiting Payment', '2' => 'Payment Confirmation', '3' => 'Ready for redeem', '4' => 'Redeemed', '11' => 'Cancel'];
			$lstatus = ['1' => 'default', '2' => 'info', '3' => 'primary', '4' => 'success', '11' => 'danger'];
			$po->preorder_status = (int)$po->preorder_status + 1;
			$po->save();

			$pushn	= Push_notification::where('customer_id', $po->customer_id)->get();

			$push_data  	= [];
			foreach($pushn as $pn){
				if(!in_array($pn->token, $push_data)){
					$push_data[]	= $pn->token;
				}
			}

			if(sizeof($push_data)){
				Pushnotification::title("Naura");
				Pushnotification::message("Status preorder ".$po->reference_no." terbaru Anda : ".$pstatus[$po->preorder_status]);
				Pushnotification::registrationsid($push_data);
				Pushnotification::send();
			}

			if($po->preorder_status == "2" || $po->preorder_status == "3"){
				$content = '<span class="label label-'.$lstatus[$po->preorder_status].'" style="cursor:pointer;" onclick="updateStatus('.$po->id.')">'.$pstatus[$po->preorder_status].'</span>';
			}
			else{
				$content = '<span class="label label-'.$lstatus[$po->preorder_status].'">'.$pstatus[$po->preorder_status].'</span>';
			}

			return $content;
		}
		else{
			return "Update failed";
		}
	}

	public function updateflag(){
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function export(){
		$store = $this->myStore();

		$list_member_id = Member::whereIn('store_id', $store)->groupBy('user_id')->pluck('user_id')->toArray();
		return $this->build_export($list_member_id);
	}
}
?>