<?php namespace digipos\Http\Controllers\Admin;
use Request;
use DB;
use Faker\Factory as Faker;


class ServicesController extends KyubiController {

	public function index(){
		$act = Request::input('act');
		return $this->$act();
	}

	private function display_image(){
		$files = Request::file('images_upload');
		$mime = ['png','jpg','gif','jpeg'];
		$ext = $files->getClientOriginalExtension();
		if (!in_array(strtolower($ext), $mime)){
			return "ext";
		}else{
			$img = 'data:image/png;base64,'.base64_encode(file_get_contents($files->getRealPath()));
			return $img;
		}
	}

	public function generate_faker(){
		$faker = Faker::create();
	    $limit = 1000;
	    $temp  = [];
	    DB::table('location')->truncate();
	    for ($i = 0; $i < $limit; $i++) {
	    	$province_id 		= $faker->numberBetween($min = 1, $max = 34);
	    	$city  				= DB::table('city')->where('province_id',$province_id)->pluck('id')->toArray();
	    	$city_id 			= $faker->randomElement($city);
	    	$sub_district  		= DB::table('sub_district')->where('city_id',$city_id)->pluck('id')->toArray();
	    	$sub_district_id 	= $faker->randomElement($sub_district);
	        $temp[]	= [
	        	'location_name' =>	$faker->unique()->streetName,
	        	'latitude' 		=>	$faker->unique()->latitude($min = -6.0, $max = -6.9),
	        	'longitude' 	=>	$faker->unique()->longitude($min = 106, $max = 109),
	        	'address' 		=>	$faker->unique()->streetAddress,
	        	'price' 		=>	$faker->randomElement([2000,4000]),
	        	'province_id'	=>  $province_id,
	        	'city_id'		=>  $city_id,
	        	'sub_district_id'	=>  $sub_district_id
	        ];
	    }
	    DB::table('location')->insert($temp);
	}
}
