<?php namespace digipos\Http\Controllers\Front;

use Illuminate\Http\request;

class AccountController extends ShukakuController {

	public function __construct(){
		parent::__construct();
	}

	public function login(request $request){
		return $this->render_view('pages.login');
	}
}
