<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Preorder extends Model{
	protected $table = 'preorder';
	protected $cinema_tickets = 'digipos\models\Cinema_tickets';
	protected $payment_methods = 'digipos\models\Payment_method';

	public function cinema_tickets(){
		return $this->belongsto($this->cinema_tickets,'cinema_ticket_id');
	}

	public function payment_methods(){
		return $this->belongsto($this->payment_methods,'payment_method');
	}
}
