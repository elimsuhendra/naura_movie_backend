<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Cinema_tickets extends Model{
	protected $table = 'cinema_tickets';
	protected $cinema = 'digipos\models\Cinema';
	protected $cinema_service = 'digipos\models\Cinema_service';
	protected $ticket = 'digipos\models\Ticket';

	public function cinema(){
		return $this->belongsto($this->cinema,'cinema_id');
	}

	public function cinema_service(){
		return $this->belongsto($this->cinema_service,'cinema_service_id');
	}
}
