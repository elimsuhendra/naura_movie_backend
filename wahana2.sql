/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : wahana

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-07-26 16:47:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `apply`
-- ----------------------------
DROP TABLE IF EXISTS `apply`;
CREATE TABLE `apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `address` text,
  `card_category_id` varchar(100) DEFAULT 'n',
  `status` varchar(100) DEFAULT 'n',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  `deleted_at` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of apply
-- ----------------------------
INSERT INTO `apply` VALUES ('4', 'Hendra2', 'Jl Gang Hj Jaimin 38A, Kemanggisan Palmerah - Jakarta Barat 114802', 'Jl Gang Hj Jaimin 38A, Kemanggisan Palmerah - Jakarta Barat 114802', '10', 'y', '2017-07-18 11:07:58', '1', null, '2017-07-06 14:33:21');

-- ----------------------------
-- Table structure for `authmenu`
-- ----------------------------
DROP TABLE IF EXISTS `authmenu`;
CREATE TABLE `authmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `user_access_id` int(11) DEFAULT NULL,
  `create` varchar(1) DEFAULT NULL,
  `edit` varchar(1) DEFAULT NULL,
  `view` varchar(1) DEFAULT NULL,
  `delete` varchar(1) DEFAULT NULL,
  `sorting` varchar(1) DEFAULT NULL,
  `export` varchar(1) DEFAULT NULL,
  `upd_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1050 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of authmenu
-- ----------------------------
INSERT INTO `authmenu` VALUES ('1049', '121', '1', 'n', 'y', 'n', 'n', 'n', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1048', '120', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1047', '135', '1', 'n', 'y', 'y', 'y', 'y', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1046', '136', '1', 'y', 'y', 'y', 'y', 'y', 'y', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1045', '143', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('565', '117', '13', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-03-22 10:49:03', '2017-03-22 10:49:03');
INSERT INTO `authmenu` VALUES ('564', '9', '13', 'y', 'n', 'y', 'n', 'n', 'y', '1', '2017-03-22 10:49:03', '2017-03-22 10:49:03');
INSERT INTO `authmenu` VALUES ('563', '7', '13', 'n', 'n', 'y', 'n', 'n', 'n', '1', '2017-03-22 10:49:03', '2017-03-22 10:49:03');
INSERT INTO `authmenu` VALUES ('585', '125', '12', 'y', 'y', 'y', 'n', 'n', 'y', '1', '2017-03-27 13:49:12', '2017-03-27 13:49:12');
INSERT INTO `authmenu` VALUES ('584', '124', '12', 'n', 'n', 'y', 'n', 'n', 'y', '1', '2017-03-27 13:49:12', '2017-03-27 13:49:12');
INSERT INTO `authmenu` VALUES ('1044', '130', '1', 'y', 'y', 'y', 'y', 'y', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('583', '122', '12', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-03-27 13:49:12', '2017-03-27 13:49:12');
INSERT INTO `authmenu` VALUES ('1043', '138', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1042', '128', '1', 'y', 'y', 'n', 'y', 'y', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1041', '133', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1040', '4', '1', 'y', 'y', 'y', 'n', 'n', 'y', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1039', '7', '1', 'y', 'y', 'y', 'n', 'n', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1038', '3', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('582', '119', '12', 'y', 'y', 'y', 'n', 'y', 'y', '1', '2017-03-27 13:49:12', '2017-03-27 13:49:12');
INSERT INTO `authmenu` VALUES ('581', '118', '12', 'y', 'y', 'y', 'n', 'n', 'y', '1', '2017-03-27 13:49:12', '2017-03-27 13:49:12');
INSERT INTO `authmenu` VALUES ('580', '112', '12', 'y', 'y', 'y', 'n', 'y', 'y', '1', '2017-03-27 13:49:12', '2017-03-27 13:49:12');
INSERT INTO `authmenu` VALUES ('579', '117', '12', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-03-27 13:49:12', '2017-03-27 13:49:12');
INSERT INTO `authmenu` VALUES ('578', '9', '12', 'y', 'n', 'y', 'n', 'n', 'y', '1', '2017-03-27 13:49:12', '2017-03-27 13:49:12');
INSERT INTO `authmenu` VALUES ('577', '7', '12', 'y', 'y', 'y', 'n', 'n', 'y', '1', '2017-03-27 13:49:12', '2017-03-27 13:49:12');
INSERT INTO `authmenu` VALUES ('576', '3', '12', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-03-27 13:49:12', '2017-03-27 13:49:12');
INSERT INTO `authmenu` VALUES ('1037', '140', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1036', '129', '1', 'n', 'y', 'y', 'n', 'y', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('575', '115', '12', 'y', 'y', 'y', 'n', 'n', 'y', '1', '2017-03-27 13:49:12', '2017-03-27 13:49:12');
INSERT INTO `authmenu` VALUES ('574', '113', '12', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-03-27 13:49:12', '2017-03-27 13:49:12');
INSERT INTO `authmenu` VALUES ('573', '1', '12', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-03-27 13:49:12', '2017-03-27 13:49:12');
INSERT INTO `authmenu` VALUES ('562', '3', '13', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-03-22 10:49:03', '2017-03-22 10:49:03');
INSERT INTO `authmenu` VALUES ('561', '115', '13', 'n', 'n', 'y', 'n', 'n', 'y', '1', '2017-03-22 10:49:03', '2017-03-22 10:49:03');
INSERT INTO `authmenu` VALUES ('560', '113', '13', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-03-22 10:49:03', '2017-03-22 10:49:03');
INSERT INTO `authmenu` VALUES ('559', '1', '13', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-03-22 10:49:03', '2017-03-22 10:49:03');
INSERT INTO `authmenu` VALUES ('566', '112', '13', 'y', 'y', 'y', 'n', 'y', 'y', '1', '2017-03-22 10:49:03', '2017-03-22 10:49:03');
INSERT INTO `authmenu` VALUES ('567', '118', '13', 'y', 'y', 'y', 'n', 'n', 'y', '1', '2017-03-22 10:49:03', '2017-03-22 10:49:03');
INSERT INTO `authmenu` VALUES ('568', '119', '13', 'y', 'y', 'y', 'y', 'y', 'y', '1', '2017-03-22 10:49:03', '2017-03-22 10:49:03');
INSERT INTO `authmenu` VALUES ('569', '122', '13', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-03-22 10:49:03', '2017-03-22 10:49:03');
INSERT INTO `authmenu` VALUES ('570', '124', '13', 'n', 'n', 'y', 'n', 'n', 'y', '1', '2017-03-22 10:49:03', '2017-03-22 10:49:03');
INSERT INTO `authmenu` VALUES ('571', '123', '13', 'n', 'y', 'y', 'n', 'n', 'y', '1', '2017-03-22 10:49:03', '2017-03-22 10:49:03');
INSERT INTO `authmenu` VALUES ('572', '125', '13', 'y', 'y', 'y', 'n', 'n', 'y', '1', '2017-03-22 10:49:03', '2017-03-22 10:49:03');
INSERT INTO `authmenu` VALUES ('1035', '127', '1', 'n', 'y', 'y', 'n', 'y', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1034', '113', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1033', '132', '1', 'y', 'y', 'y', 'y', 'y', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1032', '114', '1', 'y', 'y', 'y', 'y', 'n', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1031', '131', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');
INSERT INTO `authmenu` VALUES ('1030', '1', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-07-25 10:23:24', '2017-07-25 10:23:24');

-- ----------------------------
-- Table structure for `banner`
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `page` varchar(100) DEFAULT NULL,
  `description` text,
  `image` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO `banner` VALUES ('1', 'our-company', 'desc', 'h1JZCA_credit_card_template.jpg', '2017-02-05 01:41:44', '2017-07-24 14:11:40');
INSERT INTO `banner` VALUES ('2', 'apply', 'desc', 'HfJcXG_cardplus_logo.png', '2017-02-05 01:41:41', '2017-07-24 14:11:40');
INSERT INTO `banner` VALUES ('3', 'register', 'desc', 'DenreZ_waku_fix-02.png', '2017-02-05 01:41:39', '2017-07-24 14:11:40');
INSERT INTO `banner` VALUES ('4', 'profile-customer', 'desc', '9FxqGr_^034AEEAE1B960B7D54B0805AF98E2BCBD9DE96B9B1E33FD790^pimgpsh_fullsize_distr.jpg', '2017-02-05 01:40:48', '2017-07-24 14:10:52');
INSERT INTO `banner` VALUES ('5', 'contact-us', 'desc', 'ptHX5q_^034AEEAE1B960B7D54B0805AF98E2BCBD9DE96B9B1E33FD790^pimgpsh_fullsize_distr.jpg', null, '2017-07-24 14:10:52');

-- ----------------------------
-- Table structure for `card_category`
-- ----------------------------
DROP TABLE IF EXISTS `card_category`;
CREATE TABLE `card_category` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `card_category_name` varchar(150) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of card_category
-- ----------------------------
INSERT INTO `card_category` VALUES ('7', 'Silver', 'y', '0000-00-00 00:00:00', '2017-07-14 15:58:07', '1');
INSERT INTO `card_category` VALUES ('9', 'Platinum', 'y', '2017-07-07 16:39:34', '2017-07-14 15:57:53', '1');
INSERT INTO `card_category` VALUES ('10', 'Gold', 'y', '2017-07-14 15:47:50', '2017-07-14 15:57:39', '1');

-- ----------------------------
-- Table structure for `city`
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(10) DEFAULT NULL,
  `city_id` int(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=502 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES ('1', '21', '1', 'Aceh Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('2', '21', '2', 'Aceh Barat Daya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('3', '21', '3', 'Aceh Besar', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('4', '21', '4', 'Aceh Jaya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('5', '21', '5', 'Aceh Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('6', '21', '6', 'Aceh Singkil', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('7', '21', '7', 'Aceh Tamiang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('8', '21', '8', 'Aceh Tengah', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('9', '21', '9', 'Aceh Tenggara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('10', '21', '10', 'Aceh Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('11', '21', '11', 'Aceh Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('12', '32', '12', 'Agam', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('13', '23', '13', 'Alor', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('14', '19', '14', 'Ambon', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('15', '34', '15', 'Asahan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('16', '24', '16', 'Asmat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('17', '1', '17', 'Badung', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('18', '13', '18', 'Balangan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('19', '15', '19', 'Balikpapan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('20', '21', '20', 'Banda Aceh', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('21', '18', '21', 'Bandar Lampung', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('22', '9', '22', 'Bandung', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('23', '9', '23', 'Bandung', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('24', '9', '24', 'Bandung Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('25', '29', '25', 'Banggai', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('26', '29', '26', 'Banggai Kepulauan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('27', '2', '27', 'Bangka', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('28', '2', '28', 'Bangka Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('29', '2', '29', 'Bangka Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('30', '2', '30', 'Bangka Tengah', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('31', '11', '31', 'Bangkalan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('32', '1', '32', 'Bangli', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('33', '13', '33', 'Banjar', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('34', '9', '34', 'Banjar', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('35', '13', '35', 'Banjarbaru', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('36', '13', '36', 'Banjarmasin', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('37', '10', '37', 'Banjarnegara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('38', '28', '38', 'Bantaeng', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('39', '5', '39', 'Bantul', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('40', '33', '40', 'Banyuasin', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('41', '10', '41', 'Banyumas', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('42', '11', '42', 'Banyuwangi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('43', '13', '43', 'Barito Kuala', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('44', '14', '44', 'Barito Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('45', '14', '45', 'Barito Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('46', '14', '46', 'Barito Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('47', '28', '47', 'Barru', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('48', '17', '48', 'Batam', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('49', '10', '49', 'Batang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('50', '8', '50', 'Batang Hari', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('51', '11', '51', 'Batu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('52', '34', '52', 'Batu Bara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('53', '30', '53', 'Bau-Bau', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('54', '9', '54', 'Bekasi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('55', '9', '55', 'Bekasi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('56', '2', '56', 'Belitung', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('57', '2', '57', 'Belitung Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('58', '23', '58', 'Belu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('59', '21', '59', 'Bener Meriah', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('60', '26', '60', 'Bengkalis', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('61', '12', '61', 'Bengkayang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('62', '4', '62', 'Bengkulu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('63', '4', '63', 'Bengkulu Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('64', '4', '64', 'Bengkulu Tengah', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('65', '4', '65', 'Bengkulu Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('66', '15', '66', 'Berau', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('67', '24', '67', 'Biak Numfor', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('68', '22', '68', 'Bima', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('69', '22', '69', 'Bima', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('70', '34', '70', 'Binjai', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('71', '17', '71', 'Bintan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('72', '21', '72', 'Bireuen', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('73', '31', '73', 'Bitung', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('74', '11', '74', 'Blitar', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('75', '11', '75', 'Blitar', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('76', '10', '76', 'Blora', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('77', '7', '77', 'Boalemo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('78', '9', '78', 'Bogor', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('79', '9', '79', 'Bogor', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('80', '11', '80', 'Bojonegoro', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('81', '31', '81', 'Bolaang Mongondow (Bolmong)', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('82', '31', '82', 'Bolaang Mongondow Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('83', '31', '83', 'Bolaang Mongondow Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('84', '31', '84', 'Bolaang Mongondow Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('85', '30', '85', 'Bombana', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('86', '11', '86', 'Bondowoso', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('87', '28', '87', 'Bone', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('88', '7', '88', 'Bone Bolango', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('89', '15', '89', 'Bontang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('90', '24', '90', 'Boven Digoel', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('91', '10', '91', 'Boyolali', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('92', '10', '92', 'Brebes', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('93', '32', '93', 'Bukittinggi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('94', '1', '94', 'Buleleng', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('95', '28', '95', 'Bulukumba', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('96', '16', '96', 'Bulungan (Bulongan)', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('97', '8', '97', 'Bungo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('98', '29', '98', 'Buol', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('99', '19', '99', 'Buru', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('100', '19', '100', 'Buru Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('101', '30', '101', 'Buton', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('102', '30', '102', 'Buton Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('103', '9', '103', 'Ciamis', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('104', '9', '104', 'Cianjur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('105', '10', '105', 'Cilacap', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('106', '3', '106', 'Cilegon', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('107', '9', '107', 'Cimahi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('108', '9', '108', 'Cirebon', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('109', '9', '109', 'Cirebon', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('110', '34', '110', 'Dairi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('111', '24', '111', 'Deiyai (Deliyai)', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('112', '34', '112', 'Deli Serdang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('113', '10', '113', 'Demak', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('114', '1', '114', 'Denpasar', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('115', '9', '115', 'Depok', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('116', '32', '116', 'Dharmasraya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('117', '24', '117', 'Dogiyai', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('118', '22', '118', 'Dompu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('119', '29', '119', 'Donggala', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('120', '26', '120', 'Dumai', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('121', '33', '121', 'Empat Lawang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('122', '23', '122', 'Ende', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('123', '28', '123', 'Enrekang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('124', '25', '124', 'Fakfak', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('125', '23', '125', 'Flores Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('126', '9', '126', 'Garut', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('127', '21', '127', 'Gayo Lues', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('128', '1', '128', 'Gianyar', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('129', '7', '129', 'Gorontalo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('130', '7', '130', 'Gorontalo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('131', '7', '131', 'Gorontalo Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('132', '28', '132', 'Gowa', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('133', '11', '133', 'Gresik', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('134', '10', '134', 'Grobogan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('135', '5', '135', 'Gunung Kidul', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('136', '14', '136', 'Gunung Mas', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('137', '34', '137', 'Gunungsitoli', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('138', '20', '138', 'Halmahera Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('139', '20', '139', 'Halmahera Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('140', '20', '140', 'Halmahera Tengah', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('141', '20', '141', 'Halmahera Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('142', '20', '142', 'Halmahera Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('143', '13', '143', 'Hulu Sungai Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('144', '13', '144', 'Hulu Sungai Tengah', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('145', '13', '145', 'Hulu Sungai Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('146', '34', '146', 'Humbang Hasundutan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('147', '26', '147', 'Indragiri Hilir', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('148', '26', '148', 'Indragiri Hulu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('149', '9', '149', 'Indramayu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('150', '24', '150', 'Intan Jaya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('151', '6', '151', 'Jakarta Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('152', '6', '152', 'Jakarta Pusat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('153', '6', '153', 'Jakarta Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('154', '6', '154', 'Jakarta Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('155', '6', '155', 'Jakarta Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('156', '8', '156', 'Jambi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('157', '24', '157', 'Jayapura', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('158', '24', '158', 'Jayapura', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('159', '24', '159', 'Jayawijaya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('160', '11', '160', 'Jember', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('161', '1', '161', 'Jembrana', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('162', '28', '162', 'Jeneponto', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('163', '10', '163', 'Jepara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('164', '11', '164', 'Jombang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('165', '25', '165', 'Kaimana', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('166', '26', '166', 'Kampar', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('167', '14', '167', 'Kapuas', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('168', '12', '168', 'Kapuas Hulu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('169', '10', '169', 'Karanganyar', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('170', '1', '170', 'Karangasem', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('171', '9', '171', 'Karawang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('172', '17', '172', 'Karimun', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('173', '34', '173', 'Karo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('174', '14', '174', 'Katingan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('175', '4', '175', 'Kaur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('176', '12', '176', 'Kayong Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('177', '10', '177', 'Kebumen', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('178', '11', '178', 'Kediri', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('179', '11', '179', 'Kediri', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('180', '24', '180', 'Keerom', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('181', '10', '181', 'Kendal', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('182', '30', '182', 'Kendari', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('183', '4', '183', 'Kepahiang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('184', '17', '184', 'Kepulauan Anambas', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('185', '19', '185', 'Kepulauan Aru', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('186', '32', '186', 'Kepulauan Mentawai', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('187', '26', '187', 'Kepulauan Meranti', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('188', '31', '188', 'Kepulauan Sangihe', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('189', '6', '189', 'Kepulauan Seribu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('190', '31', '190', 'Kepulauan Siau Tagulandang Biaro (Sitaro)', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('191', '20', '191', 'Kepulauan Sula', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('192', '31', '192', 'Kepulauan Talaud', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('193', '24', '193', 'Kepulauan Yapen (Yapen Waropen)', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('194', '8', '194', 'Kerinci', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('195', '12', '195', 'Ketapang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('196', '10', '196', 'Klaten', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('197', '1', '197', 'Klungkung', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('198', '30', '198', 'Kolaka', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('199', '30', '199', 'Kolaka Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('200', '30', '200', 'Konawe', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('201', '30', '201', 'Konawe Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('202', '30', '202', 'Konawe Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('203', '13', '203', 'Kotabaru', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('204', '31', '204', 'Kotamobagu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('205', '14', '205', 'Kotawaringin Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('206', '14', '206', 'Kotawaringin Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('207', '26', '207', 'Kuantan Singingi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('208', '12', '208', 'Kubu Raya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('209', '10', '209', 'Kudus', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('210', '5', '210', 'Kulon Progo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('211', '9', '211', 'Kuningan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('212', '23', '212', 'Kupang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('213', '23', '213', 'Kupang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('214', '15', '214', 'Kutai Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('215', '15', '215', 'Kutai Kartanegara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('216', '15', '216', 'Kutai Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('217', '34', '217', 'Labuhan Batu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('218', '34', '218', 'Labuhan Batu Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('219', '34', '219', 'Labuhan Batu Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('220', '33', '220', 'Lahat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('221', '14', '221', 'Lamandau', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('222', '11', '222', 'Lamongan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('223', '18', '223', 'Lampung Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('224', '18', '224', 'Lampung Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('225', '18', '225', 'Lampung Tengah', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('226', '18', '226', 'Lampung Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('227', '18', '227', 'Lampung Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('228', '12', '228', 'Landak', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('229', '34', '229', 'Langkat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('230', '21', '230', 'Langsa', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('231', '24', '231', 'Lanny Jaya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('232', '3', '232', 'Lebak', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('233', '4', '233', 'Lebong', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('234', '23', '234', 'Lembata', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('235', '21', '235', 'Lhokseumawe', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('236', '32', '236', 'Lima Puluh Koto/Kota', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('237', '17', '237', 'Lingga', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('238', '22', '238', 'Lombok Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('239', '22', '239', 'Lombok Tengah', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('240', '22', '240', 'Lombok Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('241', '22', '241', 'Lombok Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('242', '33', '242', 'Lubuk Linggau', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('243', '11', '243', 'Lumajang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('244', '28', '244', 'Luwu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('245', '28', '245', 'Luwu Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('246', '28', '246', 'Luwu Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('247', '11', '247', 'Madiun', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('248', '11', '248', 'Madiun', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('249', '10', '249', 'Magelang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('250', '10', '250', 'Magelang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('251', '11', '251', 'Magetan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('252', '9', '252', 'Majalengka', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('253', '27', '253', 'Majene', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('254', '28', '254', 'Makassar', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('255', '11', '255', 'Malang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('256', '11', '256', 'Malang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('257', '16', '257', 'Malinau', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('258', '19', '258', 'Maluku Barat Daya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('259', '19', '259', 'Maluku Tengah', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('260', '19', '260', 'Maluku Tenggara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('261', '19', '261', 'Maluku Tenggara Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('262', '27', '262', 'Mamasa', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('263', '24', '263', 'Mamberamo Raya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('264', '24', '264', 'Mamberamo Tengah', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('265', '27', '265', 'Mamuju', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('266', '27', '266', 'Mamuju Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('267', '31', '267', 'Manado', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('268', '34', '268', 'Mandailing Natal', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('269', '23', '269', 'Manggarai', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('270', '23', '270', 'Manggarai Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('271', '23', '271', 'Manggarai Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('272', '25', '272', 'Manokwari', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('273', '25', '273', 'Manokwari Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('274', '24', '274', 'Mappi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('275', '28', '275', 'Maros', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('276', '22', '276', 'Mataram', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('277', '25', '277', 'Maybrat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('278', '34', '278', 'Medan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('279', '12', '279', 'Melawi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('280', '8', '280', 'Merangin', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('281', '24', '281', 'Merauke', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('282', '18', '282', 'Mesuji', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('283', '18', '283', 'Metro', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('284', '24', '284', 'Mimika', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('285', '31', '285', 'Minahasa', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('286', '31', '286', 'Minahasa Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('287', '31', '287', 'Minahasa Tenggara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('288', '31', '288', 'Minahasa Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('289', '11', '289', 'Mojokerto', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('290', '11', '290', 'Mojokerto', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('291', '29', '291', 'Morowali', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('292', '33', '292', 'Muara Enim', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('293', '8', '293', 'Muaro Jambi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('294', '4', '294', 'Muko Muko', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('295', '30', '295', 'Muna', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('296', '14', '296', 'Murung Raya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('297', '33', '297', 'Musi Banyuasin', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('298', '33', '298', 'Musi Rawas', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('299', '24', '299', 'Nabire', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('300', '21', '300', 'Nagan Raya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('301', '23', '301', 'Nagekeo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('302', '17', '302', 'Natuna', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('303', '24', '303', 'Nduga', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('304', '23', '304', 'Ngada', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('305', '11', '305', 'Nganjuk', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('306', '11', '306', 'Ngawi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('307', '34', '307', 'Nias', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('308', '34', '308', 'Nias Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('309', '34', '309', 'Nias Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('310', '34', '310', 'Nias Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('311', '16', '311', 'Nunukan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('312', '33', '312', 'Ogan Ilir', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('313', '33', '313', 'Ogan Komering Ilir', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('314', '33', '314', 'Ogan Komering Ulu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('315', '33', '315', 'Ogan Komering Ulu Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('316', '33', '316', 'Ogan Komering Ulu Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('317', '11', '317', 'Pacitan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('318', '32', '318', 'Padang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('319', '34', '319', 'Padang Lawas', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('320', '34', '320', 'Padang Lawas Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('321', '32', '321', 'Padang Panjang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('322', '32', '322', 'Padang Pariaman', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('323', '34', '323', 'Padang Sidempuan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('324', '33', '324', 'Pagar Alam', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('325', '34', '325', 'Pakpak Bharat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('326', '14', '326', 'Palangka Raya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('327', '33', '327', 'Palembang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('328', '28', '328', 'Palopo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('329', '29', '329', 'Palu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('330', '11', '330', 'Pamekasan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('331', '3', '331', 'Pandeglang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('332', '9', '332', 'Pangandaran', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('333', '28', '333', 'Pangkajene Kepulauan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('334', '2', '334', 'Pangkal Pinang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('335', '24', '335', 'Paniai', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('336', '28', '336', 'Parepare', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('337', '32', '337', 'Pariaman', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('338', '29', '338', 'Parigi Moutong', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('339', '32', '339', 'Pasaman', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('340', '32', '340', 'Pasaman Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('341', '15', '341', 'Paser', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('342', '11', '342', 'Pasuruan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('343', '11', '343', 'Pasuruan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('344', '10', '344', 'Pati', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('345', '32', '345', 'Payakumbuh', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('346', '25', '346', 'Pegunungan Arfak', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('347', '24', '347', 'Pegunungan Bintang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('348', '10', '348', 'Pekalongan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('349', '10', '349', 'Pekalongan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('350', '26', '350', 'Pekanbaru', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('351', '26', '351', 'Pelalawan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('352', '10', '352', 'Pemalang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('353', '34', '353', 'Pematang Siantar', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('354', '15', '354', 'Penajam Paser Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('355', '18', '355', 'Pesawaran', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('356', '18', '356', 'Pesisir Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('357', '32', '357', 'Pesisir Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('358', '21', '358', 'Pidie', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('359', '21', '359', 'Pidie Jaya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('360', '28', '360', 'Pinrang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('361', '7', '361', 'Pohuwato', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('362', '27', '362', 'Polewali Mandar', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('363', '11', '363', 'Ponorogo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('364', '12', '364', 'Pontianak', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('365', '12', '365', 'Pontianak', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('366', '29', '366', 'Poso', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('367', '33', '367', 'Prabumulih', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('368', '18', '368', 'Pringsewu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('369', '11', '369', 'Probolinggo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('370', '11', '370', 'Probolinggo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('371', '14', '371', 'Pulang Pisau', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('372', '20', '372', 'Pulau Morotai', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('373', '24', '373', 'Puncak', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('374', '24', '374', 'Puncak Jaya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('375', '10', '375', 'Purbalingga', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('376', '9', '376', 'Purwakarta', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('377', '10', '377', 'Purworejo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('378', '25', '378', 'Raja Ampat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('379', '4', '379', 'Rejang Lebong', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('380', '10', '380', 'Rembang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('381', '26', '381', 'Rokan Hilir', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('382', '26', '382', 'Rokan Hulu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('383', '23', '383', 'Rote Ndao', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('384', '21', '384', 'Sabang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('385', '23', '385', 'Sabu Raijua', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('386', '10', '386', 'Salatiga', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('387', '15', '387', 'Samarinda', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('388', '12', '388', 'Sambas', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('389', '34', '389', 'Samosir', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('390', '11', '390', 'Sampang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('391', '12', '391', 'Sanggau', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('392', '24', '392', 'Sarmi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('393', '8', '393', 'Sarolangun', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('394', '32', '394', 'Sawah Lunto', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('395', '12', '395', 'Sekadau', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('396', '28', '396', 'Selayar (Kepulauan Selayar)', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('397', '4', '397', 'Seluma', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('398', '10', '398', 'Semarang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('399', '10', '399', 'Semarang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('400', '19', '400', 'Seram Bagian Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('401', '19', '401', 'Seram Bagian Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('402', '3', '402', 'Serang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('403', '3', '403', 'Serang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('404', '34', '404', 'Serdang Bedagai', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('405', '14', '405', 'Seruyan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('406', '26', '406', 'Siak', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('407', '34', '407', 'Sibolga', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('408', '28', '408', 'Sidenreng Rappang/Rapang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('409', '11', '409', 'Sidoarjo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('410', '29', '410', 'Sigi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('411', '32', '411', 'Sijunjung (Sawah Lunto Sijunjung)', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('412', '23', '412', 'Sikka', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('413', '34', '413', 'Simalungun', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('414', '21', '414', 'Simeulue', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('415', '12', '415', 'Singkawang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('416', '28', '416', 'Sinjai', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('417', '12', '417', 'Sintang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('418', '11', '418', 'Situbondo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('419', '5', '419', 'Sleman', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('420', '32', '420', 'Solok', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('421', '32', '421', 'Solok', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('422', '32', '422', 'Solok Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('423', '28', '423', 'Soppeng', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('424', '25', '424', 'Sorong', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('425', '25', '425', 'Sorong', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('426', '25', '426', 'Sorong Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('427', '10', '427', 'Sragen', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('428', '9', '428', 'Subang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('429', '21', '429', 'Subulussalam', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('430', '9', '430', 'Sukabumi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('431', '9', '431', 'Sukabumi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('432', '14', '432', 'Sukamara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('433', '10', '433', 'Sukoharjo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('434', '23', '434', 'Sumba Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('435', '23', '435', 'Sumba Barat Daya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('436', '23', '436', 'Sumba Tengah', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('437', '23', '437', 'Sumba Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('438', '22', '438', 'Sumbawa', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('439', '22', '439', 'Sumbawa Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('440', '9', '440', 'Sumedang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('441', '11', '441', 'Sumenep', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('442', '8', '442', 'Sungaipenuh', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('443', '24', '443', 'Supiori', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('444', '11', '444', 'Surabaya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('445', '10', '445', 'Surakarta (Solo)', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('446', '13', '446', 'Tabalong', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('447', '1', '447', 'Tabanan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('448', '28', '448', 'Takalar', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('449', '25', '449', 'Tambrauw', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('450', '16', '450', 'Tana Tidung', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('451', '28', '451', 'Tana Toraja', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('452', '13', '452', 'Tanah Bumbu', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('453', '32', '453', 'Tanah Datar', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('454', '13', '454', 'Tanah Laut', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('455', '3', '455', 'Tangerang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('456', '3', '456', 'Tangerang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('457', '3', '457', 'Tangerang Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('458', '18', '458', 'Tanggamus', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('459', '34', '459', 'Tanjung Balai', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('460', '8', '460', 'Tanjung Jabung Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('461', '8', '461', 'Tanjung Jabung Timur', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('462', '17', '462', 'Tanjung Pinang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('463', '34', '463', 'Tapanuli Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('464', '34', '464', 'Tapanuli Tengah', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('465', '34', '465', 'Tapanuli Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('466', '13', '466', 'Tapin', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('467', '16', '467', 'Tarakan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('468', '9', '468', 'Tasikmalaya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('469', '9', '469', 'Tasikmalaya', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('470', '34', '470', 'Tebing Tinggi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('471', '8', '471', 'Tebo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('472', '10', '472', 'Tegal', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('473', '10', '473', 'Tegal', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('474', '25', '474', 'Teluk Bintuni', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('475', '25', '475', 'Teluk Wondama', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('476', '10', '476', 'Temanggung', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('477', '20', '477', 'Ternate', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('478', '20', '478', 'Tidore Kepulauan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('479', '23', '479', 'Timor Tengah Selatan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('480', '23', '480', 'Timor Tengah Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('481', '34', '481', 'Toba Samosir', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('482', '29', '482', 'Tojo Una-Una', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('483', '29', '483', 'Toli-Toli', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('484', '24', '484', 'Tolikara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('485', '31', '485', 'Tomohon', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('486', '28', '486', 'Toraja Utara', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('487', '11', '487', 'Trenggalek', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('488', '19', '488', 'Tual', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('489', '11', '489', 'Tuban', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('490', '18', '490', 'Tulang Bawang', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('491', '18', '491', 'Tulang Bawang Barat', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('492', '11', '492', 'Tulungagung', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('493', '28', '493', 'Wajo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('494', '30', '494', 'Wakatobi', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('495', '24', '495', 'Waropen', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('496', '18', '496', 'Way Kanan', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('497', '10', '497', 'Wonogiri', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('498', '10', '498', 'Wonosobo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('499', '24', '499', 'Yahukimo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('500', '24', '500', 'Yalimo', '2016-06-03 08:12:54', '0000-00-00 00:00:00');
INSERT INTO `city` VALUES ('501', '5', '501', 'Yogyakarta', '2016-06-03 08:12:54', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `client`
-- ----------------------------
DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(100) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` varchar(100) DEFAULT NULL,
  `meta_keyword` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of client
-- ----------------------------
INSERT INTO `client` VALUES ('1', 'Honda Infinite Card', null, 'y', '2017-07-05 14:41:55', '2017-07-07 14:17:45', '1', null, null, null);
INSERT INTO `client` VALUES ('2', 'Honda VIP Card', null, 'y', '2017-07-05 13:24:11', '2017-07-07 14:17:52', '1', null, null, null);
INSERT INTO `client` VALUES ('3', ' IML Super Tyre Sealent', null, 'y', '2017-07-05 14:41:26', '2017-07-07 14:17:54', '1', null, null, null);
INSERT INTO `client` VALUES ('5', 'Sakura Spa', null, 'y', '0000-00-00 00:00:00', '2017-07-07 14:19:14', '1', null, null, null);
INSERT INTO `client` VALUES ('6', 'GEN Fm', null, 'y', '0000-00-00 00:00:00', '2017-07-07 14:19:15', '1', null, null, null);
INSERT INTO `client` VALUES ('7', 'Kyochon', null, 'y', '0000-00-00 00:00:00', '2017-07-07 14:19:15', '1', null, null, null);
INSERT INTO `client` VALUES ('8', 'Laboratorium Gunung Sahari', null, 'y', '0000-00-00 00:00:00', '2017-07-07 14:19:15', '1', null, null, null);
INSERT INTO `client` VALUES ('9', 'MBC SPA', null, 'y', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `client` VALUES ('10', 'SM Pertamina', null, 'y', '0000-00-00 00:00:00', '2017-07-07 14:19:17', '1', null, null, null);
INSERT INTO `client` VALUES ('13', 'Client 10', 'GPg9ww_special_promo.jpg', 'y', '2017-07-14 16:20:32', '2017-07-14 18:08:03', '1', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)');
INSERT INTO `client` VALUES ('14', 'Client 3', 'mj9zfK_toyota_logo.jpg', 'y', '2017-07-14 16:21:18', '2017-07-17 09:08:06', '1', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)');

-- ----------------------------
-- Table structure for `client_card`
-- ----------------------------
DROP TABLE IF EXISTS `client_card`;
CREATE TABLE `client_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) DEFAULT NULL,
  `id_card_category` int(11) DEFAULT NULL,
  `client_card_name` varchar(100) DEFAULT NULL,
  `no_card` varchar(100) DEFAULT NULL,
  `code_card` varchar(100) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `description` text,
  `image` varchar(100) DEFAULT NULL,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` varchar(100) DEFAULT NULL,
  `meta_keyword` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of client_card
-- ----------------------------
INSERT INTO `client_card` VALUES ('1', null, null, 'About Us', null, null, null, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', null, 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '2017-07-05 14:41:55', '2017-07-07 09:19:28', '1');
INSERT INTO `client_card` VALUES ('2', null, null, 'Vision', null, null, null, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', null, 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '2017-07-05 13:24:11', '2017-07-07 09:19:16', '1');
INSERT INTO `client_card` VALUES ('3', null, null, 'Mission', null, null, null, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'TFwPRN_toyota_logo.jpg', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '2017-07-05 14:41:26', '2017-07-14 13:35:28', '1');
INSERT INTO `client_card` VALUES ('4', '13', '10', 'name1', 'nocard1', 'code1', '2018-07-18', 'desc1', 'Ve0l4w_special_promo.jpg', null, null, null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `client_card` VALUES ('24', '14', '7', 'Toyota Silver Card', '', '', '2017-07-24', 'description 3', 'BFPUiZ_credit_card_template.jpg', null, null, null, '2017-07-24 00:00:00', '2017-07-24 09:46:27', '1');
INSERT INTO `client_card` VALUES ('25', '14', '7', 'Hendra', '', '', '2017-07-24', 'desc2', '7Xnoj5_special_promo.jpg', null, null, null, '2017-07-24 00:00:00', '2017-07-24 09:46:27', '1');
INSERT INTO `client_card` VALUES ('26', '14', '7', 'Elim Suhendra', '', '', '2017-07-24', 'desc 1', '3G4Iyv_rPoHrY_aladien.jpg', null, null, null, '2017-07-24 00:00:00', '2017-07-24 09:46:27', '1');

-- ----------------------------
-- Table structure for `config`
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `value` text,
  `config_type` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('1', 'web_name', 'Card Plus', 'both', '2017-02-05 01:41:44', '2017-07-17 15:35:40');
INSERT INTO `config` VALUES ('2', 'web_logo', 'CGop8q_cardplus_logo.png', 'both', '2017-02-05 01:41:41', '2017-07-17 15:35:41');
INSERT INTO `config` VALUES ('3', 'web_email', 'elim@solveway.co.id', 'both', '2017-02-05 01:41:39', '2017-07-17 15:35:40');
INSERT INTO `config` VALUES ('4', 'favicon', 'Up5Zj8_cardplus_logo.png', 'both', '2017-02-05 01:40:48', '2017-07-17 15:35:41');
INSERT INTO `config` VALUES ('5', 'web_title', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'both', '2017-02-05 08:27:57', '2017-07-06 17:55:22');
INSERT INTO `config` VALUES ('6', 'web_description', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'both', '2017-02-05 08:28:00', '2017-07-06 17:55:21');
INSERT INTO `config` VALUES ('7', 'web_keywords', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'both', '2017-02-05 08:28:03', '2017-07-06 17:55:25');
INSERT INTO `config` VALUES ('8', 'maintenance_mode', 'n', 'front', '2017-02-05 08:28:06', '2017-02-05 08:28:06');
INSERT INTO `config` VALUES ('9', 'terms_&_condition', null, 'front', '2017-02-05 08:28:09', '2017-02-05 08:28:13');
INSERT INTO `config` VALUES ('10', 'expired_length', '30', 'front', '2017-02-05 08:28:16', '2017-02-05 08:28:17');
INSERT INTO `config` VALUES ('11', 'facebook_key', '1808407302780488', 'front', '2017-02-05 08:28:19', '2017-02-05 08:28:20');
INSERT INTO `config` VALUES ('12', 'google_android_key', '942734714734-m2tkmhhrp3ngkd5e2pvcj3522tbelb9g.apps.googleusercontent.com', 'front', '2017-02-05 08:28:22', '2017-02-05 08:28:22');

-- ----------------------------
-- Table structure for `customer`
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `address` text,
  `email` varchar(100) DEFAULT 'n',
  `password` varchar(100) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `ttl` varchar(100) DEFAULT NULL,
  `no_machine` varchar(100) DEFAULT NULL,
  `no_card` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'n',
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  `deleted_at` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES ('12', 'Elim Suhendra', '08999878045', 'Jl. ABC No. 9', 'elimsuhendra@gmail.com', '$2y$10$aoqsvBHiNoELx4Ah3SXWDeqtadBU8B.lJhqI7aeJSs.nzHRAM9Oqm', 'l7EQzi_waku_fix-02.png', 'Jakarta, 01-10-1994', '987654321', '123456789', 'y', '2017-07-18 16:17:43', '2017-07-18 17:49:49', '1', null);

-- ----------------------------
-- Table structure for `footer`
-- ----------------------------
DROP TABLE IF EXISTS `footer`;
CREATE TABLE `footer` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `value` text,
  `image` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of footer
-- ----------------------------
INSERT INTO `footer` VALUES ('1', 'operational_hour', '<p>Senin - jum\'at, 09:00 s/d 17:00 wib.</p><p>Sabtu, 09:00 s/d 15:00 wib.</p><p>Minggu / hari besar - tutup.</p>', null, '2017-02-05 01:41:44', '2017-07-24 15:10:46');

-- ----------------------------
-- Table structure for `merchant_category`
-- ----------------------------
DROP TABLE IF EXISTS `merchant_category`;
CREATE TABLE `merchant_category` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(150) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of merchant_category
-- ----------------------------
INSERT INTO `merchant_category` VALUES ('1', 'Food & Beverage', 'y', '0000-00-00 00:00:00', '2017-07-07 16:04:41', null);
INSERT INTO `merchant_category` VALUES ('2', 'Cafe', 'y', '0000-00-00 00:00:00', '2017-07-07 16:04:41', null);
INSERT INTO `merchant_category` VALUES ('3', 'Entertainment', 'y', '0000-00-00 00:00:00', '2017-07-07 16:04:41', null);
INSERT INTO `merchant_category` VALUES ('4', 'Health', 'y', '0000-00-00 00:00:00', '2017-07-07 16:04:41', null);
INSERT INTO `merchant_category` VALUES ('5', 'Beauty', 'y', '0000-00-00 00:00:00', '2017-07-07 16:04:41', null);
INSERT INTO `merchant_category` VALUES ('6', 'Shopping', 'y', '0000-00-00 00:00:00', '2017-07-07 16:04:41', null);
INSERT INTO `merchant_category` VALUES ('7', 'Otomotif', 'y', '0000-00-00 00:00:00', '2017-07-07 16:04:41', '1');
INSERT INTO `merchant_category` VALUES ('9', 'cat 22', 'y', '2017-07-07 16:39:34', '2017-07-07 16:44:21', '1');

-- ----------------------------
-- Table structure for `merchant_log`
-- ----------------------------
DROP TABLE IF EXISTS `merchant_log`;
CREATE TABLE `merchant_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` varchar(100) DEFAULT NULL,
  `ip_address` text,
  `city` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  `deleted_at` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of merchant_log
-- ----------------------------
INSERT INTO `merchant_log` VALUES ('4', '6', '192.168.1.10', 'jakarta', 'y', '2017-07-06 14:33:21', '2017-07-24 17:29:45', '1', null);
INSERT INTO `merchant_log` VALUES ('5', '5', '192.168.1.10', 'jakarta', 'y', '2017-07-23 17:28:48', '2017-07-24 17:30:16', '1', null);
INSERT INTO `merchant_log` VALUES ('6', '6', '192.168.1.10', 'jakarta', 'y', '2017-07-24 17:29:53', '2017-07-25 16:48:49', '1', null);
INSERT INTO `merchant_log` VALUES ('7', '7', '192.168.1.10', 'bandung', 'y', '2017-07-25 11:27:44', '2017-07-25 11:27:50', '1', null);
INSERT INTO `merchant_log` VALUES ('8', '5', '192.168.1.10', 'jakarta', 'y', '2017-07-25 16:49:33', '2017-07-25 16:49:37', '1', null);

-- ----------------------------
-- Table structure for `mslanguage`
-- ----------------------------
DROP TABLE IF EXISTS `mslanguage`;
CREATE TABLE `mslanguage` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `language_name` varchar(30) DEFAULT NULL,
  `language_name_alias` varchar(30) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `order` int(10) DEFAULT NULL,
  `upd_by` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mslanguage
-- ----------------------------
INSERT INTO `mslanguage` VALUES ('1', 'Indonesia', 'id', 'y', '1', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `mslanguage` VALUES ('2', 'English', 'en', 'y', '2', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `msmenu`
-- ----------------------------
DROP TABLE IF EXISTS `msmenu`;
CREATE TABLE `msmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `heading` varchar(30) DEFAULT NULL,
  `menu_name` varchar(50) DEFAULT NULL,
  `menu_name_alias` varchar(50) DEFAULT NULL,
  `menu_id` int(10) DEFAULT NULL,
  `status_parent` varchar(1) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `create` varchar(1) DEFAULT NULL,
  `edit` varchar(1) DEFAULT NULL,
  `view` varchar(1) DEFAULT NULL,
  `delete` varchar(1) DEFAULT NULL,
  `sorting` varchar(1) DEFAULT NULL,
  `export` varchar(1) DEFAULT NULL,
  `order` int(2) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=144 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of msmenu
-- ----------------------------
INSERT INTO `msmenu` VALUES ('1', 'Control Menu', 'Dashboard', '/', null, 'y', 'fa fa-home', 'n', 'n', 'n', 'n', 'n', 'n', '1', 'y', '2017-02-05 09:47:53', '2017-02-05 09:47:54');
INSERT INTO `msmenu` VALUES ('7', null, 'User', 'user', '3', 'n', null, 'y', 'y', 'y', 'y', 'n', 'n', '2', 'y', '2017-02-05 09:51:18', '2017-07-25 10:24:17');
INSERT INTO `msmenu` VALUES ('4', null, 'Access Right', 'access-right', '3', 'n', null, 'y', 'y', 'y', 'y', 'n', 'n', '1', 'y', '2017-02-05 09:51:21', '2017-07-25 10:24:17');
INSERT INTO `msmenu` VALUES ('3', null, 'Users', 'users', null, 'y', 'fa fa-users', 'y', 'y', 'y', 'n', 'y', 'n', '4', 'y', '2017-02-05 09:51:29', '2017-07-25 10:14:51');
INSERT INTO `msmenu` VALUES ('124', null, 'Inbox', 'inbox', '122', 'n', null, 'n', 'n', 'y', 'n', 'n', 'n', '2', 'y', '0000-00-00 00:00:00', '2017-07-25 10:24:22');
INSERT INTO `msmenu` VALUES ('133', null, 'Our Clients', 'our-clients', null, 'y', 'fa fa-credit-card', 'y', 'y', 'y', 'y', 'y', 'n', '5', 'y', '0000-00-00 00:00:00', '2017-07-25 10:14:53');
INSERT INTO `msmenu` VALUES ('134', null, 'Card Category', 'card-category', '133', 'n', null, 'y', 'y', 'y', 'y', 'y', 'n', '2', 'n', '0000-00-00 00:00:00', '2017-07-25 10:24:14');
INSERT INTO `msmenu` VALUES ('113', null, 'Page', 'page', null, 'y', 'fa fa-bars', 'n', 'n', 'n', 'n', 'n', 'n', '3', 'y', '2017-02-06 23:55:39', '2017-07-20 16:11:27');
INSERT INTO `msmenu` VALUES ('114', null, 'Manage', 'manage-merchant', '131', 'n', null, 'y', 'y', 'y', 'y', 'n', 'n', '1', 'y', '2017-02-06 23:55:41', '2017-07-25 10:24:13');
INSERT INTO `msmenu` VALUES ('136', null, 'Customer', 'customer', '143', 'n', null, 'y', 'y', 'y', 'y', 'y', 'n', '2', 'y', '0000-00-00 00:00:00', '2017-07-25 10:24:12');
INSERT INTO `msmenu` VALUES ('137', null, 'footer', 'footer', '140', 'n', null, 'n', 'y', 'y', 'n', 'n', 'n', '1', 'y', '0000-00-00 00:00:00', '2017-07-25 10:24:11');
INSERT INTO `msmenu` VALUES ('120', null, 'Preferences', 'preferences', null, 'y', 'fa fa-cogs', 'n', 'n', 'n', 'n', 'n', 'n', '20', 'y', '0000-00-00 00:00:00', '2017-03-20 03:40:52');
INSERT INTO `msmenu` VALUES ('121', null, 'General-settings', 'general-settings', '120', 'n', null, 'n', 'y', 'n', 'n', 'n', 'n', '2', 'y', '0000-00-00 00:00:00', '2017-07-20 16:49:06');
INSERT INTO `msmenu` VALUES ('138', null, 'News', 'news', null, 'y', 'fa fa-newspaper-o', 'n', 'n', 'n', 'n', 'n', 'n', '6', 'y', '0000-00-00 00:00:00', '2017-07-24 15:16:01');
INSERT INTO `msmenu` VALUES ('139', null, 'Account', 'account', null, 'y', 'fa fa-account', 'n', 'n', 'n', 'n', 'n', 'n', '7', null, '0000-00-00 00:00:00', null);
INSERT INTO `msmenu` VALUES ('135', null, 'Apply', 'apply', '143', 'n', '', 'n', 'y', 'y', 'y', 'y', 'n', '1', 'y', '0000-00-00 00:00:00', '2017-07-25 10:21:33');
INSERT INTO `msmenu` VALUES ('127', null, 'Our Services', 'our-service', '113', 'n', '', 'n', 'y', 'y', 'n', 'y', 'n', '3', 'y', '0000-00-00 00:00:00', '2017-07-25 10:21:35');
INSERT INTO `msmenu` VALUES ('128', null, 'Manage', 'manage-our-client', '133', 'n', '', 'y', 'y', 'y', 'y', 'y', 'n', '4', 'y', '0000-00-00 00:00:00', '2017-07-25 10:21:37');
INSERT INTO `msmenu` VALUES ('129', null, 'Our Company', 'our-company', '113', 'n', '', 'n', 'y', 'y', 'n', 'y', 'n', '5', 'y', '0000-00-00 00:00:00', '2017-07-25 10:21:38');
INSERT INTO `msmenu` VALUES ('130', null, 'Manage', 'manage-news', '138', 'n', '', 'y', 'y', 'y', 'y', 'y', 'n', '1', 'y', '0000-00-00 00:00:00', '2017-07-25 10:15:01');
INSERT INTO `msmenu` VALUES ('131', null, 'Merchants', 'merchants', null, 'y', 'fa fa-flag', 'n', 'n', 'n', 'n', 'n', 'n', '2', 'y', '0000-00-00 00:00:00', '2017-07-05 17:17:27');
INSERT INTO `msmenu` VALUES ('132', null, 'Category', 'merchant-category', '131', 'n', '', 'y', 'y', 'y', 'y', 'y', 'n', '3', 'y', '0000-00-00 00:00:00', '2017-07-25 10:21:40');
INSERT INTO `msmenu` VALUES ('140', null, 'Home', 'home', '113', 'n', null, 'n', 'n', 'n', 'n', 'n', 'n', '1', 'y', '0000-00-00 00:00:00', '2017-07-20 17:20:21');
INSERT INTO `msmenu` VALUES ('143', null, 'Accounts', 'accounts', null, 'y', 'fa fa-user', 'y', 'y', 'y', 'y', 'y', 'n', '19', 'y', '0000-00-00 00:00:00', '2017-07-25 10:15:03');

-- ----------------------------
-- Table structure for `msmerchant`
-- ----------------------------
DROP TABLE IF EXISTS `msmerchant`;
CREATE TABLE `msmerchant` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `merchant_name` varchar(100) DEFAULT NULL,
  `status` varchar(2) DEFAULT 'y',
  `address` text,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `term_and_condition` text,
  `id_kecamatan` int(11) DEFAULT NULL,
  `id_kelurahan` int(11) DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_merchant_category` int(11) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` varchar(100) DEFAULT NULL,
  `meta_keyword` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of msmerchant
-- ----------------------------
INSERT INTO `msmerchant` VALUES ('5', 'Toyota Jakarta', 'y', 'Jl. Abc No 9', 'toyotajakarta@gmail.com', '08999123123', null, null, null, '151', '7', 'BzPxN1_toyota_logo.jpg', '2017-07-11 15:51:54', '2017-07-12 15:33:10', '1', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)');
INSERT INTO `msmerchant` VALUES ('6', 'Toyota Bandung', 'y', 'Jl. xyz No 8', 'toyotabandung@gmail.com', '8999878045', null, null, null, '153', '7', 'kRObwR_toyota_logo.jpg', '2017-07-11 15:55:58', '2017-07-12 15:33:22', '1', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)');
INSERT INTO `msmerchant` VALUES ('7', 'Sakura Spa', 'y', 'Jl. xyz', 'sakuraspa@gmail.com', '08999878046', null, null, null, '151', '4', 'BzPxN1_toyota_logo.jpg', '2017-07-25 11:26:51', '2017-07-25 11:26:54', null, null, null, null);

-- ----------------------------
-- Table structure for `news`
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `image` varchar(100) DEFAULT NULL,
  `headline_news` varchar(100) DEFAULT 'n',
  `sticky_news` varchar(100) DEFAULT 'n',
  `status` varchar(100) DEFAULT NULL,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` varchar(100) DEFAULT NULL,
  `meta_keyword` varchar(100) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  `deleted_at` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('4', 'news1', 'news1', '1tFg2i_5qPesS_Hydrangeas.jpg', 'n', 'n', 'y', null, null, null, '2017-07-17 16:54:51', '1', null, '2017-07-06 14:33:21');
INSERT INTO `news` VALUES ('5', 'news 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'mn5sCy_2BbYrj_CLOUD.png', 'n', 'n', 'y', null, null, null, '2017-07-17 16:54:52', '1', '', '2017-07-06 14:34:02');
INSERT INTO `news` VALUES ('6', 'news 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '8Twg7m_5qPesS_Hydrangeas.jpg', 'n', 'n', 'y', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '2017-07-17 16:54:54', '1', null, '2017-07-06 14:37:05');
INSERT INTO `news` VALUES ('7', 'news 4', 'news 4', 'UWm8hf_Chrysanthemum.jpg', 'n', 'n', 'y', '{\"id\":5,\"name\":\"web_title\",\"value\":\"PT Wahana Kalyanamitra Mahardhika (CardPlus)\",\"config_type\":\"bot', '{\"id\":6,\"name\":\"web_description\",\"value\":\"PT Wahana Kalyanamitra Mahardhika (CardPlus)\",\"config_type', null, '2017-07-17 16:54:55', '1', null, '2017-07-06 18:27:32');
INSERT INTO `news` VALUES ('8', 'news 5', 'news 5', null, 'n', 'n', 'y', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '2017-07-17 16:54:56', '1', null, '2017-07-06 18:29:58');
INSERT INTO `news` VALUES ('9', 'news 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', null, 'n', 'n', 'y', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '2017-07-17 16:54:56', '1', null, '2017-07-06 18:53:21');
INSERT INTO `news` VALUES ('10', 'n', 'news 4', null, 'n', 'n', 'y', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '2017-07-17 16:54:58', '1', null, '2017-07-07 09:20:30');
INSERT INTO `news` VALUES ('11', 'news 6', 'desc 6', 'uagZDL_rPoHrY_aladien.jpg', 'y', 'n', 'y', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '2017-07-17 17:10:41', '1', null, '2017-07-17 17:00:27');

-- ----------------------------
-- Table structure for `news_log`
-- ----------------------------
DROP TABLE IF EXISTS `news_log`;
CREATE TABLE `news_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` varchar(100) DEFAULT NULL,
  `ip_address` text,
  `city` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  `deleted_at` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of news_log
-- ----------------------------
INSERT INTO `news_log` VALUES ('4', 'news1', 'news1', '1tFg2i_5qPesS_Hydrangeas.jpg', 'y', '2017-07-06 14:33:21', '2017-07-17 16:54:51', '1', null);
INSERT INTO `news_log` VALUES ('5', 'news 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'mn5sCy_2BbYrj_CLOUD.png', 'y', '2017-07-06 14:34:02', '2017-07-17 16:54:52', '1', '');
INSERT INTO `news_log` VALUES ('6', 'news 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '8Twg7m_5qPesS_Hydrangeas.jpg', 'y', '2017-07-06 14:37:05', '2017-07-17 16:54:54', '1', null);
INSERT INTO `news_log` VALUES ('7', 'news 4', 'news 4', 'UWm8hf_Chrysanthemum.jpg', 'y', '2017-07-06 18:27:32', '2017-07-17 16:54:55', '1', null);
INSERT INTO `news_log` VALUES ('8', 'news 5', 'news 5', null, 'y', '2017-07-06 18:29:58', '2017-07-17 16:54:56', '1', null);
INSERT INTO `news_log` VALUES ('9', 'news 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', null, 'y', '2017-07-06 18:53:21', '2017-07-17 16:54:56', '1', null);
INSERT INTO `news_log` VALUES ('10', 'n', 'news 4', null, 'y', '2017-07-07 09:20:30', '2017-07-17 16:54:58', '1', null);
INSERT INTO `news_log` VALUES ('11', 'news 6', 'desc 6', 'uagZDL_rPoHrY_aladien.jpg', 'y', '2017-07-17 17:00:27', '2017-07-17 17:10:41', '1', null);

-- ----------------------------
-- Table structure for `our_company`
-- ----------------------------
DROP TABLE IF EXISTS `our_company`;
CREATE TABLE `our_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `our_company_name` varchar(100) DEFAULT NULL,
  `description` text,
  `image` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` varchar(100) DEFAULT NULL,
  `meta_keyword` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of our_company
-- ----------------------------
INSERT INTO `our_company` VALUES ('1', 'About Us', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', null, 'y', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '2017-07-05 14:41:55', '2017-07-07 09:19:28', '1');
INSERT INTO `our_company` VALUES ('2', 'Vision', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', null, 'y', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '2017-07-05 13:24:11', '2017-07-07 09:19:16', '1');
INSERT INTO `our_company` VALUES ('3', 'Mission', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'TFwPRN_toyota_logo.jpg', 'y', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '2017-07-05 14:41:26', '2017-07-14 13:35:28', '1');

-- ----------------------------
-- Table structure for `our_service`
-- ----------------------------
DROP TABLE IF EXISTS `our_service`;
CREATE TABLE `our_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `our_service_name` varchar(100) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `description` text,
  `status` varchar(100) DEFAULT NULL,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` varchar(100) DEFAULT NULL,
  `meta_keyword` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of our_service
-- ----------------------------
INSERT INTO `our_service` VALUES ('1', 'Customer Database Management', null, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'y', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '2017-07-05 14:41:55', '2017-07-14 14:15:52', '1');
INSERT INTO `our_service` VALUES ('2', 'Membership Management', null, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'y', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '2017-07-05 13:24:11', '2017-07-14 14:15:23', '1');
INSERT INTO `our_service` VALUES ('3', 'Promotional Communication Service', null, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>', 'y', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '2017-07-05 14:41:26', '2017-07-14 14:15:00', '1');
INSERT INTO `our_service` VALUES ('4', 'Merchant Network', null, '<p>description</p>', 'y', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '0000-00-00 00:00:00', '2017-07-14 14:14:34', '1');
INSERT INTO `our_service` VALUES ('5', 'Point Reward Program', 'V8AsN4_cardplus_logo.png', '<p>description</p>', 'y', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', 'PT Wahana Kalyanamitra Mahardhika (CardPlus)', '0000-00-00 00:00:00', '2017-07-25 10:12:27', '1');

-- ----------------------------
-- Table structure for `outlet`
-- ----------------------------
DROP TABLE IF EXISTS `outlet`;
CREATE TABLE `outlet` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `merchant_id` int(15) NOT NULL,
  `outlet_name` varchar(100) DEFAULT NULL,
  `address` text,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=262 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of outlet
-- ----------------------------
INSERT INTO `outlet` VALUES ('259', '6', 'Toyota Bandung1', 'Jl tekad no 1', '021 111', 'toyotabandung1@gmail.com', '0000-00-00 00:00:00', '2017-07-14 11:26:48', '1');
INSERT INTO `outlet` VALUES ('260', '6', 'Toyota Bandung2', 'Jl tekad no 2', '021 112', 'toyotabandung2@gmail.com', '0000-00-00 00:00:00', '2017-07-14 11:26:48', '1');
INSERT INTO `outlet` VALUES ('261', '6', 'Toyota Bandung 3', 'Jl tekad no 3', '', '', '0000-00-00 00:00:00', '2017-07-14 11:26:48', '1');

-- ----------------------------
-- Table structure for `promo`
-- ----------------------------
DROP TABLE IF EXISTS `promo`;
CREATE TABLE `promo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promo_name` varchar(100) DEFAULT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  `description` text,
  `image` varchar(100) DEFAULT NULL,
  `term_condition` text,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT 'y',
  `deleted_at` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of promo
-- ----------------------------
INSERT INTO `promo` VALUES ('34', 'Promo Toyota Bandung 1', '6', 'xz', '5OkrLd_special_promo.jpg', 'xz', '2017-07-14', '2017-07-30', 'y', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- ----------------------------
-- Table structure for `province`
-- ----------------------------
DROP TABLE IF EXISTS `province`;
CREATE TABLE `province` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of province
-- ----------------------------
INSERT INTO `province` VALUES ('1', '1', 'Bali', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('2', '2', 'Bangka Belitung', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('3', '3', 'Banten', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('4', '4', 'Bengkulu', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('5', '5', 'DI Yogyakarta', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('6', '6', 'DKI Jakarta', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('7', '7', 'Gorontalo', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('8', '8', 'Jambi', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('9', '9', 'Jawa Barat', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('10', '10', 'Jawa Tengah', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('11', '11', 'Jawa Timur', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('12', '12', 'Kalimantan Barat', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('13', '13', 'Kalimantan Selatan', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('14', '14', 'Kalimantan Tengah', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('15', '15', 'Kalimantan Timur', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('16', '16', 'Kalimantan Utara', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('17', '17', 'Kepulauan Riau', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('18', '18', 'Lampung', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('19', '19', 'Maluku', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('20', '20', 'Maluku Utara', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('21', '21', 'Nanggroe Aceh Darussalam (NAD)', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('22', '22', 'Nusa Tenggara Barat (NTB)', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('23', '23', 'Nusa Tenggara Timur (NTT)', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('24', '24', 'Papua', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('25', '25', 'Papua Barat', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('26', '26', 'Riau', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('27', '27', 'Sulawesi Barat', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('28', '28', 'Sulawesi Selatan', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('29', '29', 'Sulawesi Tengah', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('30', '30', 'Sulawesi Tenggara', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('31', '31', 'Sulawesi Utara', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('32', '32', 'Sumatera Barat', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('33', '33', 'Sumatera Selatan', '2016-06-03 07:59:30', '0000-00-00 00:00:00');
INSERT INTO `province` VALUES ('34', '34', 'Sumatera Utara', '2016-06-03 07:59:30', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `seo_setting`
-- ----------------------------
DROP TABLE IF EXISTS `seo_setting`;
CREATE TABLE `seo_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of seo_setting
-- ----------------------------
INSERT INTO `seo_setting` VALUES ('1', 'meta_title', 'PT. DUO MULTI SOLUSINDO');
INSERT INTO `seo_setting` VALUES ('2', 'meta_description', 'PT. DUO MULTI SOLUSINDO');
INSERT INTO `seo_setting` VALUES ('3', 'meta_keyword', 'PT. Duo Multi Solusindo, Panel Surya');

-- ----------------------------
-- Table structure for `slideshow`
-- ----------------------------
DROP TABLE IF EXISTS `slideshow`;
CREATE TABLE `slideshow` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `image` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slideshow
-- ----------------------------
INSERT INTO `slideshow` VALUES ('1', 'slideshow 1', null, 'Udqi7H_credit_card_template.jpg', null, '2017-07-24 15:07:00');
INSERT INTO `slideshow` VALUES ('2', 'slideshow 2', null, 'LTV9FW_^034AEEAE1B960B7D54B0805AF98E2BCBD9DE96B9B1E33FD790^pimgpsh_fullsize_distr.jpg', null, '2017-07-24 14:10:50');
INSERT INTO `slideshow` VALUES ('3', 'slideshow 3', null, '', null, '2017-07-24 15:07:00');

-- ----------------------------
-- Table structure for `social_media`
-- ----------------------------
DROP TABLE IF EXISTS `social_media`;
CREATE TABLE `social_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(12) NOT NULL,
  `value` text NOT NULL,
  `icon` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of social_media
-- ----------------------------
INSERT INTO `social_media` VALUES ('1', 'facebook', 'https://www.facebook.com/PT-Solveway-Digital-Indonesia-2101773256715028/', 'fb.png', null, '2017-07-25 10:15:29');
INSERT INTO `social_media` VALUES ('5', 'twitter', 'https://twitter.com/SolvewayDI', '', null, '2017-07-25 10:15:29');
INSERT INTO `social_media` VALUES ('6', 'instagram', 'https://www.instagram.com/solveway/', '', null, '2017-07-25 10:15:29');
INSERT INTO `social_media` VALUES ('7', 'linkedin', 'https://www.facebook.com/PT-Solveway-Digital-Indonesia-2101773256715028/', '', null, '2017-07-25 10:15:29');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picture` varchar(100) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT 'New User',
  `status` varchar(2) NOT NULL DEFAULT 'y',
  `user_access_id` varchar(10) DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `language_id` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'Qkx8aX_cardplus_logo.png', 'admin', 'admin@admin.com', '$2y$10$CiZ32spsYo6wvkvBXFxguO1YvU3oQdK39JAtP3AMK1xnEKt5v1i0C', '5hkWyKEw27zzhfl53KiIPVbeeJjDN1Q0bd2lHQl3lfiHHFK7NsTl2x6EMJmn', '08123456', 'admin', 'y', '1', null, '2', null, '2017-07-25 10:18:21');
INSERT INTO `user` VALUES ('60', 'R7F2t4_cardplus_logo.png', 'elim', 'elim@solveway.co.id', '$2y$10$p214ZYYupldttf7TK6fJ/uZspU/0KYlHh85YaNh1goVPTUxxD2Pmy', null, '08999878046', 'elim', 'y', '12', '1', '1', '2017-07-19 13:43:36', '2017-07-19 14:17:39');

-- ----------------------------
-- Table structure for `useraccess`
-- ----------------------------
DROP TABLE IF EXISTS `useraccess`;
CREATE TABLE `useraccess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_name` varchar(40) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of useraccess
-- ----------------------------
INSERT INTO `useraccess` VALUES ('1', 'Super Admin', '2017-02-05 09:52:17', '2017-02-05 09:52:19');
INSERT INTO `useraccess` VALUES ('12', 'Admin', '2017-03-06 14:41:00', '2017-03-06 14:41:00');
